# README #

### What is this application for? ###

* Quick summary: this in an android application where you will find tones of awesome facts. 
this application can help you to become more educated and interesting person

* Version: 1.0 alpha

### How do I use this application? ###

After downloading it you can start learning.

First window will show you facts about the universe.

To change category of facts, tap to the burger menu on left top corner (or swipe from the very left side to right) and choose category.

By tapping on the text of the fact it will be copied.

To add fact to favorites (that category is in the menu, above category "universe"), you should be signed in your account.
If you are in account, tap the star in the right side of fact and fact will be added to favorites

To create an account, first you should register by tapping register button in menu (above).
Then enter your E-mail, password and repeat password. The password must be at least 9 characters long and contain at least one digit.
Then tap "register" button.

If you already have an account, choose "sign in" in menu and enter your E-mail and password that you used during registration
and tap "sign in" button.

If you have an account, but you forgot password, tap "sign in" button in the menu, then tap "sorgot password button", then 
enter E-mail of your account and tap "sign in" button. link to reset the password will be sent to that E-mail address.

If you are in account and want to change password, tap "change password" button in menu, then enter your current password and then 
enter your new password. Tap "change password" button.

If you are in account but want to sign out, tap "sign out" button in the menu. Now you are signed out and can create anothea account.

### for developers ###

Dependencies:
    implementation 'androidx.core:core-ktx:1.7.0'
    implementation 'androidx.appcompat:appcompat:1.4.0'
    implementation 'androidx.constraintlayout:constraintlayout:2.1.2'
    implementation 'com.google.android.material:material:1.5.0'
    implementation 'com.google.code.gson:gson:2.8.6'
    implementation 'androidx.legacy:legacy-support-v4:1.0.0'
    implementation 'com.google.firebase:firebase-auth-ktx:21.0.1'
    testImplementation 'junit:junit:4.+'
    androidTestImplementation 'androidx.test.ext:junit:1.1.3'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.4.0'

### Who do I talk to? ###
* Repo owner or admin: Giorgi Lomidze and Luka Kontselidze