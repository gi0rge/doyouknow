package com.example.doyouknow

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class changepasswordFragment : Fragment(R.layout.fragment_changepassword) {
    private lateinit var userCurrentPassword: EditText
    private lateinit var userNewPassword: EditText
    private lateinit var changePasswordBtn: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userCurrentPassword = view.findViewById(R.id.userCurrentPassword)
        userNewPassword = view.findViewById(R.id.userNewPassword)
        changePasswordBtn = view.findViewById(R.id.changePasswordBtn)

        fun changePassword() {
            val currentPassText = userCurrentPassword.text.toString()
            val newPassText = userNewPassword.text.toString()
            val sharedPreferences: SharedPreferences = requireActivity()
                .getSharedPreferences("currentUserPass", Context.MODE_PRIVATE)
            val currentUserPass = sharedPreferences.getString("currentUserPass", null)

            if (currentPassText != currentUserPass) {
                val message = "Passwords do not match"
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                return;
            } else if (userNewPassword.text.trim().length < 9 || Regex("(?=.*[0-9])") !in newPassText) {
                val message = "The password must be at least 9 characters long and contain at least one digit"
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                return;
            }

            FirebaseAuth.getInstance().currentUser?.updatePassword(newPassText)
                ?.addOnCompleteListener { task ->
                    if(task.isSuccessful) {
                        val message = "Password changed successfully"
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                        val fragmentFrame = view.parent as ViewGroup
                        fragmentFrame.visibility = View.GONE
                    } else {
                        Toast.makeText(context, "an error occurred", Toast.LENGTH_SHORT).show()
                    }
                }
        }

        changePasswordBtn.setOnClickListener{
            changePassword()
        }

    }
}