package com.example.doyouknow

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView

import com.example.doyouknow.facts.planets.planetFactsTextList
import com.example.doyouknow.facts.universe.universeFactsTextList
import com.example.doyouknow.facts.animals.animalFactsTextList
import com.example.doyouknow.facts.celebrities.celebrityFactsTextList
import com.example.doyouknow.facts.countries.countryFactsTextList
import com.example.doyouknow.facts.health.healthFactsTextList
import com.example.doyouknow.facts.IT.ITFactsTextList
import com.example.doyouknow.facts.sport.sportFactsTextList
import com.example.doyouknow.facts.human.humanFactsTextList
import com.example.doyouknow.facts.history.historyFactsTextList
import com.example.doyouknow.facts.all.allFactsTextList
import com.example.doyouknow.facts.favorites.favoritesFactsTextList
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson

import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type



class MainActivity : AppCompatActivity() {
    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navView: NavigationView
    private lateinit var rcView: RecyclerView
    private lateinit var frame: FrameLayout
    private val adapter = FactAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        navView.setNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.planets -> drawFactsFrom(planetFactsTextList)
                R.id.celebrities -> drawFactsFrom(celebrityFactsTextList)
                R.id.animals -> drawFactsFrom(animalFactsTextList)
                R.id.countries -> drawFactsFrom(countryFactsTextList)
                R.id.health -> drawFactsFrom(healthFactsTextList)
                R.id.IT -> drawFactsFrom(ITFactsTextList)
                R.id.universe -> drawFactsFrom(universeFactsTextList)
                R.id.sport -> drawFactsFrom(sportFactsTextList)
                R.id.human -> drawFactsFrom(humanFactsTextList)
                R.id.history -> drawFactsFrom(historyFactsTextList)
                R.id.all -> drawFactsFrom(allFactsTextList)
                R.id.favorites -> {
                    drawFactsFrom(favoritesFactsTextList)
                    letKnowIfNoFavorites()
                }
            }
            true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(toggle.onOptionsItemSelected(item)) return true
        return super.onOptionsItemSelected(item)
    }

    private fun init() {
        rcView = findViewById(R.id.rcView)
        drawerLayout = findViewById(R.id.drawerLayout)
        navView = findViewById(R.id.navView)
        toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close)
        frame = findViewById(R.id.fragmentFrame)

        navView.itemIconTintList = null
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        navView.menu.getItem(1).isChecked = true;

        rcView.layoutManager = LinearLayoutManager(this@MainActivity)
        rcView.adapter = adapter
        drawFactsFrom(universeFactsTextList)

        if(FirebaseAuth.getInstance().currentUser != null) {
            loadFavorites()
            val openRegistrationBtn = navView.getHeaderView(0).findViewById<Button>(R.id.openRegistrationBtn)
            val openAuthenticationBtn = navView.getHeaderView(0).findViewById<Button>(R.id.openAuthenticationBtn)
            val changePassBtn = navView.getHeaderView(0).findViewById<Button>(R.id.changePassBtn)
            val signOutBtn = navView.getHeaderView(0).findViewById<Button>(R.id.signOutBtn)
            val userSignedInInfo = navView.getHeaderView(0).findViewById<TextView>(R.id.userSignedInInfo)

            userSignedInInfo.text = FirebaseAuth.getInstance().currentUser?.email.toString()
            openRegistrationBtn.visibility = View.GONE
            openAuthenticationBtn.visibility = View.GONE
            changePassBtn.visibility = View.VISIBLE
            signOutBtn.visibility = View.VISIBLE
        }
    }

   private fun drawFactsFrom(list: List<String>) {
        frame.visibility = View.GONE
        adapter.factsList.clear()
        for (i in list.indices) {
            val fact = Fact(list[i])
            adapter.addFact(fact)
        }
        rcView.getLayoutManager()?.scrollToPosition(0)
        adapter.notifyDataSetChanged()
        drawerLayout.closeDrawer(GravityCompat.START)
    }

     fun addTextToClipboard(view: View) {
        if(view is TextView){
            val textToCopy = view.text
            val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clipData = ClipData.newPlainText("text", textToCopy)
            clipboardManager.setPrimaryClip(clipData)
            Toast.makeText(this, "Text copied to clipboard", Toast.LENGTH_LONG).show()
        }
    }

    fun addToFavorites(view: View) {
        if(FirebaseAuth.getInstance().currentUser != null && view is ImageView) {
            val containingLayout = view.parent as ViewGroup
            val textView = containingLayout.getChildAt(0)

            if(textView is TextView) {
                view.setImageResource(R.drawable.favorite_checked)
                val text = textView.text.toString()

                if(!favoritesFactsTextList.contains(text)) {
                    favoritesFactsTextList.add(text)
                    saveFavorites()
                    Toast.makeText(applicationContext, "Added to Favorites!", Toast.LENGTH_SHORT).show()
                } else {
                    view.setImageResource(R.drawable.favorite_unchecked)
                    favoritesFactsTextList.remove(text)
                    saveFavorites()
                    Toast.makeText(applicationContext, "Removed from Favorites!", Toast.LENGTH_SHORT).show()
                }
            }
        } else {
            val message = "You should have account to unlock favorites"
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun saveFavorites() {
        if(FirebaseAuth.getInstance().currentUser != null) {
            val sharedPreferences = getSharedPreferences(FirebaseAuth.getInstance().currentUser?.email.toString(), MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            val gson = Gson()
            val json = gson.toJson(favoritesFactsTextList)
            editor.putString(FirebaseAuth.getInstance().currentUser?.email.toString(), json)
            editor.apply()
        }
    }

    private fun loadFavorites() {
        val sharedPreferences = getSharedPreferences(FirebaseAuth.getInstance().currentUser?.email.toString(), MODE_PRIVATE)
        val gson = Gson()
        val json = sharedPreferences.getString(
            FirebaseAuth.getInstance().currentUser?.email.toString(),
            null
        )
        val type: Type = object : TypeToken<ArrayList<String>>() {}.type
        try {
            favoritesFactsTextList = gson.fromJson(json, type)
        } catch (e: Exception) {
            return
        }
    }

    private fun letKnowIfNoFavorites() {
        if (FirebaseAuth.getInstance().currentUser == null) {
            val message = "You should have account to unlock favorites"
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
        } else if (favoritesFactsTextList.size == 0) {
            Toast.makeText(applicationContext, "You don't have favorites yet", Toast.LENGTH_SHORT).show()
        }
    }

    fun openRegistrationFragment(view: View) {
        frame.visibility = View.VISIBLE
        supportFragmentManager.beginTransaction().apply {
        replace(R.id.fragmentFrame, registerFragment())
        commit()
        drawerLayout.closeDrawer(GravityCompat.START)
       }
   }

    fun openAuthorizationFragment(view: View) {
        frame.visibility = View.VISIBLE
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentFrame, signInFragment())
            commit()
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    fun openResetPasswordFragment(view: View) {
        frame.visibility = View.VISIBLE
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentFrame, resetPasswordFragment())
            commit()
        }
    }

    fun openChangePasswordFragment(view: View) {
        frame.visibility = View.VISIBLE
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentFrame, changepasswordFragment())
            commit()
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    fun signOut(view: View) {
        FirebaseAuth.getInstance().signOut()
        try {
            val openRegistrationBtn = navView.getHeaderView(0).findViewById<Button>(R.id.openRegistrationBtn)
            val openAuthenticationBtn = navView.getHeaderView(0).findViewById<Button>(R.id.openAuthenticationBtn)
            val changePassBtn = navView.getHeaderView(0).findViewById<Button>(R.id.changePassBtn)
            val signOutBtn = navView.getHeaderView(0).findViewById<Button>(R.id.signOutBtn)
            val userSignedInInfo = navView.getHeaderView(0).findViewById<TextView>(R.id.userSignedInInfo)

            loadFavorites()
            favoritesFactsTextList.clear()
            drawFactsFrom(universeFactsTextList)
            userSignedInInfo.text = "Not signed in"
            openRegistrationBtn.visibility = View.VISIBLE
            openAuthenticationBtn.visibility = View.VISIBLE
            changePassBtn.visibility = View.GONE
            signOutBtn.visibility = View.GONE
            val message = "You signed out successfully"
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            val message = "You signed out. Please reload application"
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
        }
    }
}