package com.example.doyouknow

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth

class resetPasswordFragment : Fragment(R.layout.fragment_reset_password) {
    private lateinit var userEmail: EditText
    private lateinit var resetPasswordBtn: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userEmail = view.findViewById(R.id.userEmail)
        resetPasswordBtn = view.findViewById(R.id.resetPasswordBtn)

        fun sendEmailToResetPass() {
            val emailText = userEmail.text.toString()

            if ("@" !in emailText || "." !in emailText) {
                val message = "There is no such e-mail"
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                return;
            }

            FirebaseAuth.getInstance().sendPasswordResetEmail(emailText)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful) {
                        val message = "Check your E-mail"
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                        val fragmentFrame = view.parent as ViewGroup
                        fragmentFrame.visibility = View.GONE
                    } else {
                        Toast.makeText(context, "an error occurred", Toast.LENGTH_SHORT).show()
                    }
                }
        }

        resetPasswordBtn.setOnClickListener{
            sendEmailToResetPass()
        }

    }
}