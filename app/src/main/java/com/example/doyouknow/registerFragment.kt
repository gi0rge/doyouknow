package com.example.doyouknow

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import android.content.SharedPreferences

import android.content.Context.MODE_PRIVATE





class registerFragment : Fragment() {
    private lateinit var userEmail: EditText
    private lateinit var userPassword: EditText
    private lateinit var submitRegistrationBtn: Button
    private lateinit var userPasswordRepeat: EditText


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userEmail = view.findViewById(R.id.userEmail)
        userPassword = view.findViewById(R.id.userPassword)
        submitRegistrationBtn = view.findViewById(R.id.submitRegistrationBtn)
        userPasswordRepeat = view.findViewById(R.id.userPasswordRepeat)

        val activityMain = view.parent.parent.parent as ViewGroup
        val navView = activityMain.findViewById<NavigationView>(R.id.navView).getHeaderView(0)

        val openRegistrationBtn = navView.findViewById<Button>(R.id.openRegistrationBtn)
        val openAuthenticationBtn = navView.findViewById<Button>(R.id.openAuthenticationBtn)
        val userSignedInInfo = navView.findViewById<TextView>(R.id.userSignedInInfo)
        val signOutBtn = navView.findViewById<Button>(R.id.signOutBtn)
        val changePassBtn = navView.findViewById<Button>(R.id.changePassBtn)

        fun checkReg() {
            val passText = userPassword.text.toString()
            val emailText = userEmail.text.toString()
            val passRepeatText = userPasswordRepeat.text.toString()


            if ("@" !in emailText || "." !in emailText) {
                val message = "There is no such e-mail"
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                return;
            } else if (userPassword.text.trim().length < 9 || Regex("(?=.*[0-9])") !in passText) {
                val message = "The password must be at least 9 characters long and contain at least one digit"
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                return;
            } else if(passRepeatText != passText) {
                val message = "Passwords do not match"
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                return;
            }

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(emailText, passText)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful) {
                        val sharedPreferences: SharedPreferences = requireActivity()
                            .getSharedPreferences("currentUserPass", MODE_PRIVATE)
                        val editor = sharedPreferences.edit()
                        editor.putString("currentUserPass", passText)
                        editor.apply()

                        userSignedInInfo.text = emailText
                        openRegistrationBtn.visibility = View.GONE
                        openAuthenticationBtn.visibility = View.GONE
                        signOutBtn.visibility = View.VISIBLE
                        changePassBtn.visibility = View.VISIBLE
                        val message = "You registered successfully"
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                        val fragmentFrame = view.parent as ViewGroup
                        fragmentFrame.visibility = View.GONE
                    } else {
                        Toast.makeText(context, "an error occurred", Toast.LENGTH_SHORT).show()
                    }
                }
        }

        submitRegistrationBtn.setOnClickListener{
            checkReg()
        }

    }

}