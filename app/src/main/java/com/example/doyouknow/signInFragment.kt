package com.example.doyouknow

import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.doyouknow.facts.favorites
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class signInFragment : Fragment(R.layout.fragment_sign_in) {
    private lateinit var userEmail: EditText
    private lateinit var userPassword: EditText
    private lateinit var submitAuthorizationBtn: Button
    private lateinit var forgotPassBtn: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userEmail = view.findViewById(R.id.userEmail)
        submitAuthorizationBtn = view.findViewById(R.id.submitAuthorizationBtn)
        userPassword = view.findViewById(R.id.userPassword)
        forgotPassBtn = view.findViewById(R.id.forgotPassBtn)

        val activityMain = view.parent.parent.parent as ViewGroup
        val navView = activityMain.findViewById<NavigationView>(R.id.navView).getHeaderView(0)

        val openRegistrationBtn = navView.findViewById<Button>(R.id.openRegistrationBtn)
        val openAuthenticationBtn = navView.findViewById<Button>(R.id.openAuthenticationBtn)
        val userSignedInInfo = navView.findViewById<TextView>(R.id.userSignedInInfo)
        val signOutBtn = navView.findViewById<Button>(R.id.signOutBtn)
        val changePassBtn = navView.findViewById<Button>(R.id.changePassBtn)

        fun checkAuthorization() {
            val passText = userPassword.text.toString()
            val emailText = userEmail.text.toString()

            if ("@" !in emailText || "." !in emailText) {
                val message = "There is no such e-mail"
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                return;
            } else if (userPassword.text.trim().length < 9 || Regex("(?=.*[0-9])") !in passText) {
                val message = "The password must be at least 9 characters long and contain at least one digit"
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                return;
            }

            FirebaseAuth.getInstance().signInWithEmailAndPassword(emailText, passText)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful) {
                        loadFavorites()
                        val sharedPreferences: SharedPreferences = requireActivity()
                            .getSharedPreferences("currentUserPass", MODE_PRIVATE)
                        val editor = sharedPreferences.edit()
                        editor.putString("currentUserPass", passText)
                        editor.apply()

                        userSignedInInfo.text = emailText
                        openRegistrationBtn.visibility = View.GONE
                        openAuthenticationBtn.visibility = View.GONE
                        signOutBtn.visibility = View.VISIBLE
                        changePassBtn.visibility = View.VISIBLE
                        val message = "You signed in successfully"
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                        val fragmentFrame = view.parent as ViewGroup
                        fragmentFrame.visibility = View.GONE
                    } else {
                        Toast.makeText(context, "an error occurred", Toast.LENGTH_SHORT).show()
                    }
                }
        }

        submitAuthorizationBtn.setOnClickListener{
            checkAuthorization()
        }
    }

    private fun loadFavorites() {
        if(FirebaseAuth.getInstance().currentUser != null) {
            val sharedPreferences = requireActivity().getSharedPreferences(FirebaseAuth.getInstance().currentUser?.email.toString(),MODE_PRIVATE
            )
            val gson = Gson()
            val json = sharedPreferences.getString(
                FirebaseAuth.getInstance().currentUser?.email.toString(),
                null
            )
            val type: Type = object : TypeToken<ArrayList<String>>() {}.type
            try {
                favorites.favoritesFactsTextList = gson.fromJson(json, type)
            } catch (e: Exception) {
                return
            }
        }
    }
}