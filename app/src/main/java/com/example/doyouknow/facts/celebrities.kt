package com.example.doyouknow.facts

object celebrities {
    val celebrityFactsTextList = listOf(
        "Three weeks before they started recording The Walking Dead, Andrew Lincoln (who plays Rick Grimes) went out and practiced his accent by ordering coffees & fried chicken!",

        "Bruno Mars‘ real name is Peter Gene Hernandez.",

        "Eminem’s song “Lose Yourself” was the first rap song to win an Oscar for Best Original Song, however, instead of watching the awards, Eminem was asleep watching cartoons with his daughter.",

        "Bryan Cranston’s parents didn’t want him to have a career in Hollywood, so he didn’t start pursuing his acting career until he left college. You may also know him as Walter White from the hit television show Breaking Bad.",

        "Robert Downey Jr. claims that Burger King saved his life from his drug addiction.",

        "In March 2006, Beyoncé accepted a star on the Hollywood Walk of Fame.",

        "Nathan Sykes from the British boy-band “The Wanted” used to have a rabbit called Pikachu when he was younger.",

        "On August 16, 2005, Madonna fell off a horse at her home outside London; she suffered from 3 cracked ribs, a broken collarbone, and a broken hand.",

        "Brad Pitt first gained recognition as a cowboy hitchhiker in the road movie Thelma & Louise.",

        "Angelina Jolie performed her own stunts whilst filming Lara Croft: Tomb Raider.",

        "For the movie “The Scorpion King,” Dwayne Johnson earned \$5.5 million and registered his name in the Guinness World Records for receiving the highest salary for a first-time leading role.",

        "Uncle Phil from Fresh Prince of Bel-Air was also the voice of Shredder from the original Teenage Mutant Ninja Turtles cartoon.",

        "Although Emma Watson is British, she was actually born in Paris, France on April 15, 1990.",

        "In 2011, Jennifer Lawrence appeared in The Beaver with Mel Gibson, Jodie Foster, and Anton Yelchin, and played Mystique in X-Men: First Class.",

        "Orlando Bloom learned how to surf in New Zealand whilst filming “The Lord of the Rings.”",

        "Tobey Maguire, who played Spider-Man in the 2002 film, had a fear of heights.",

        "Taylor Swift is extremely talented, she can play the guitar, piano, ukulele, electric guitar, and the banjo!",

        "George Lucas originally wanted to be a race car driver, but an accident just before his high school graduation changed his mind.",

        "Leonardo DiCaprio has never done drugs and had to speak to a drug expert to prepare for Wolf of Wall Street.",

        "Jennifer Lopez was the first actress to have a movie and an album hit number one in the same week.",

        "At age 15, Cristiano Ronaldo was diagnosed with a heart condition that required surgery, he was briefly sidelined and made a full recovery.",

        "Ryan Gosling was asked to audition for boy-band Backstreet Boys but he turned it down.",

        "Ben Affleck starred in, directed, and produced the movie “Argo” in 2012, which earned him awards for acting, directing, producing and writing.",

        "Before becoming a movie star, Harrison Ford was a master carpenter – a craft he still does as a hobby.",

        "Miley Cyrus first tried out for Hannah Montana at the age of 11. She was denied due to her age.",

        "As a child, Jim Carrey wore tap shoes to bed just in case his parents needed cheering up in the middle of the night.",

        "Megan Fox was banned from the Wal-Mart store in her hometown because she shoplifted makeup as a teenager. She claims it was an accident.",

        "Eminem originally wanted to become a comic-book artist.",

        "Evangeline Lilly is a notoriously bad driver. She has been in at least 8 car accidents, and none of them were with another car.",

        "Colin Farrell says that Marilyn Monroe was the first woman he fell in love with.",

        "Michael Jackson loved Mexican food; later in his life he loved KFC’s fried chicken.",

        "Elvis Presley’s daughter, Lisa Marie, became the wife of Michael Jackson on 26th May 1994, but they divorced after 4 years.",

        "Kanye West’s song “Hey Mama” was dedicated to his mother, who died in 2007 after complications with plastic surgery.",

        "Willow Smith’s musical debut is 8 years earlier than her fathers (Will Smith), making her the youngest debuting superstar in her family.",

        "Cameron Diaz had no previous acting experience before she got the part in The Mask.",

        "Daniel Radcliffe broke over 80 wands while filming the Harry Potter movies because he used them as drumsticks!",
    )
}