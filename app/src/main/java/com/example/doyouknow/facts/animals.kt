package com.example.doyouknow.facts

object animals {
    val animalFactsTextList = listOf(
        "To the untrained eye, jaguars can be mistaken for leopards, but you can tell the difference from their rosettes (circular markings): Jaguars have black dots in the middle of some of their rosettes, whereas leopards don’t. Jaguars also have larger, rounded heads and short legs. ",

        "Sumatran rhinos are the smallest, but they can still weigh 600kg (that’s almost 95 stone). And white rhinos are the largest, weighing up to 3,500kg (over 550 stone, or well over 3 tonnes!).",

        "Orangutans are incredibly dexterous and use both hands and feet while gathering food and travelling through the trees.",

        "Sometimes, to mark their scent, panda's climb a tree backwards with their hindfeet until they're in a full handstand upside down - enabling them to leave their scent higher up. ",

        "Tigers' main prey is deer but they also eat wild boar. For tigers, only one in ten hunts are successful, a large deer can provide a tiger with one week's food.",

        "The African elephant is the world's largest land mammal – with males on average measuring up to 3m high and weighing up to 6 tonnes.",

        "Elephants have around 150,000 muscle units in their trunk. Their trunks are perhaps the most sensitive organ found in any mammal - Asian elephants have been seen to pick up a peanut, shell it, blow the shell out and eat the nut.",

        "Nearly all wild lions live in Africa, below the Sahara Desert, but one small population exists around Gir Forest National Park in western India.",

        "This means that exposure to human illnesses – even a cold - can have potentially detrimental impacts on gorillas as they are so genetically similar to us, but they haven't developed the necessary immunities.",

        "We share around 98% of our DNA with gorillas. This means that exposure to human illnesses – even a cold - can have potentially detrimental impacts on gorillas as they are so genetically similar to us, but they haven't developed the necessary immunities. ",

        "Turtles don't have teeth. They use their beak-like mouth to grasp their food. This beak is made of keratin (the same stuff your fingernails are made of).",

        "Sharks live in most ocean habitats. They can be found in beautiful, tropical coral reefs, to the deep sea, and even under the arctic sea ice.",

        "Bottlenose dolphins are one of the few species, along with apes and humans, that have the ability to recognise themselves in a mirror. This is considered 'reflective' of their intelligence. Dolphins are also among the few animals that have been documented using tools. In Shark Bay in Western Australia, dolphins fit marine sponges over their beaks to protect them from sharp, harmful rocks as they forage for fish.",

        "Humpback whales in the Southern Hemisphere live off their fat reserves for 5.5-7.5 months each year, as they migrate from their tropical breeding grounds to the Antarctic, to feed on krill.",

        "Whale sharks can grow up to 12 metres long. But despite their size, whale sharks are often referred to as \"gentle giants\".",

        "Emperors are the biggest of the 18 species of penguin found today, and one of the largest of all birds. They are approximately 120cm tall (about the height of a six year old child) and weigh in at around 40 kg, though their weight does fluctuate dramatically throughout the year.",

        "Polar bear fur is translucent, and only appears white because it reflects visible light. Beneath all that thick fur, their skin is jet black.",

        "Unlike other big cats, snow leopards can't roar. Snow leopards have a 'main' call described as a 'piercing yowl' that's so loud it can be heard over the roar of a river.",
    )

}