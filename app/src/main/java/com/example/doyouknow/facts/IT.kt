package com.example.doyouknow.facts

object IT {
    val ITFactsTextList = listOf(
        "The JAVA language was previously called Oak. It was named after an oak tree that was grown outside James Gosling’s office (Gosling is best known as the founder and lead designer behind the Java coding language). Later, it was renamed Java, from Java coffee (a kind of coffee from Indonesia).",

        "It’s a common misbelief that the Firefox logo is a fox (I mean… it is in the name), but it is actually a red panda!",

        "There are around 700 separate programming languages",

        "PHP language was not created to be used worldwide for enterprise applications. Rasmus Lerdorf, the creator of PHP, built this coding language just to manage his personal web project! But, with time PHP became one of the popular programming languages in the market.",

        "According to many online studies, the most disliked programming languages are Perl, Delphi, and VBA",

        "Recent studies show that around 70% of coding jobs have nothing to do with technology at all",

        "The world’s first computer programmer was a renowned female mathematician",

        "Computer Programming was instrumental in helping end World War II",

        "Did you know? Fortran (FORmula TRANslation) was the first coding language created by John Backus and his team at IBM in the 1950s.",

        "There are 3 very different types of Hackers, one being malicious, the other benevolent, and the last somewhere in between the two",

        "The first-ever computer game made zero profit for its team of creators",

        "NASA still uses programs from the 70s in their spacecraft",

        "Coding is behind almost everything that is powered by electricity.",

        "In June 1991, James Gosling, Mike Sheridan, and Patrick Naughton began the Java language project. This programming language was originally created for interactive television, but this language was too modern for the digital cable television sector at that period.",

        "In this Covid-19 pandemic time, Avi Schiffmann, a teenager, developed one of the world’s most popular COVID-19 monitoring websites. He coded ncov2019.live, the admired covid virus tracker that is one of the most visited corona trackers across the globe.",

        "Did you know? ADA is a coding language built in 1980 that is used by the International Space Station. In 1995, this programming language was accepted as an international standard programming language.",

        "In 1980, MIT mathematician and a co-founder of the coding language LOGO, Seymour Papert published a book “Mindstorms: Children, Computers and Powerful Ideas” in 1980.  The book tells, kids can enhance their cognitive thinking skills by learning to code.",

        "Bill Gates, Co-founder of Microsoft, created his first computer program- an implementation of tic-tac-toe that enabled users to play games against the computer.",

        "Go is a programming language that was designed at Google in 2007 to enhance coding productivity in the time of multicore, networked machines and large codebases.",

        "Alan Turing invented the “Turing Test” (one of the Turing tests is CAPTCHA). It is a set of twisted numbers or words on online forms that assist to identify humans from computers. This CAPTCHA was also the contribution of Alan Turing.",

        "Markus Persson, a Swedish software programmer, developed and launched the computer game- Minecraft in 2009. Later the computer game became so popular that Microsoft bought the game for \$2.5 billion. ",

        "Steve Jobs and his partner Steve Wozniak began their career by building a computer arcade game named “Breakout.”",

        "Before creating the “C”, one of the most well-known programming languages, there was a predecessor coding language known as the “B”. This “B” language was written by Ken Thompson, a Turing Award-winning computer scientist. Later, Dennis Ritchie created the “C” language by improving the “B” language.",

        "Fred Cohen is known as the inventor of computer virus defense techniques. In 1983, he developed a parasitic application that could ‘infect’ computer systems. He called it a computer virus.",

        "Java is both a compiler and an interpreter programming language. The reason being, source code is first compiled into a binary byte-code. After that, byte-code runs on the Java Virtual Machine (a software-based interpreter).",

        "JavaScript programming language is called a dynamically typed language because a programmer can create new variables at runtime, and variable type is set at runtime.",

        "Python programming language for Data Science returns more than one value from a single function unlike in the majority of the modern programming languages.",

        "You cannot represent infinity as an integer in any coding language. But with python, you can represent an infinite integer via float values.",
    )

}