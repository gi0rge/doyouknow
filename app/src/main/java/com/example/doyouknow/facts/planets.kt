package com.example.doyouknow.facts

object planets {
     val planetFactsTextList = listOf(
         "Mercury was named for the swift-running Roman messenger god Mercury, because the planet is the fastest in the race around the sun. A year on Mercury is 88 Earth days, the shortest orbit in the solar system.",

         "Mercury's mantra is “the days are long but the years are short.” One day-night cycle on Mercury can take 176 Earth days — or more than two Mercury years — because of the planet's slow roll and elliptical orbit.",

         "Mercury is not the hottest planet in the solar system, despite being the closest to the sun! Turns out Mercury is both extremely hot and and extremely cold: Daytime temperatures on the side of the planet facing the sun reach 800 degrees Fahrenheit, but the side facing away from the sun gets all the way down to minus 290 degrees Fahrenheit.",

         "Believe it or not, scientists have found actual ice on Mercury! Because the planet has very little atmosphere (thanks to its proximity to the sun) and its axis has very little tilt (less than one degree) there are permanently-shadowed craters at Mercury's north and south poles that contain ice.",

         "Due to its thick atmosphere of toxic gases, Venus retains heat; its temperatures of 900 degrees Fahrenheit are the hottest in the solar system. Of course the planet named for the Roman god of love and beauty is a hottie!",

         "Days on Venus are loooooooong — it takes 243 Earth days for Venus to fully rotate. A Venusian day is also slightly longer than one Venusian year, which lasts 225 Earth days.",

         "It’s always backwards day on Venus, because the planet spins from east to west, the opposite direction from most of the other planets that orbit the sun.",

         "The surface of Venus is covered with mountains and volcanoes, some of which might still be active. It’s hard to get to the surface of Venus, though, because the air pressure is so intense it could crush you!",

         "Earth is unusual in our solar system for several reasons: It’s the only planet with liquid water at its surface, with known life, and with one moon. It’s also the only planet not named after a Roman god: The word “earth” comes from the Old English and Germanic word for “ground.”",

         "The earth is actually tilted 23 degrees. This tilt is why we have seasons: When the northern hemisphere is tilted toward the sun, we get summer; when it’s tilted away, we have winter.",

         "Earth’s moon is the fifth largest moon in the entire solar system! It’s useful, too, causing the tides and stabilizing the planet’s wobble, which creates a more predictable climate.",

         "It takes roughly eight minutes for light to reach Earth from the sun.",

         "Mars boasts the solar system’s largest known volcano, Olympus Mons (named for Mount Olympus, home of the gods). It’s three times the height of Mount Everest. ",

         "The Valles Marineris (seen above) is a Martian canyon that’s 10 times the size of the Grand Canyon. It is 3,000 miles long, 200 miles wide, and 4.3 miles deep (at its widest and deepest spots). For comparison, that's longer than the entire United States!",

         "Mars actually has two lumpy moons named Phobos and Deimos. Phobos isn’t holding up so well, and scientists predict it could crash into the planet or break up into a ring around the planet. Don’t wait up for it, though — it won’t happen for another 40 or 50 million years.",

         "The planet’s blood-red color — caused by the oxidation (rusting) of iron in its soil — earned it the name Mars after the bloodthirsty Roman god of war.",

         "Jupiter is the biggest planet in the solar system and therefore named for the king of the Roman gods. If you combined the mass of all the other planets, then doubled it, Jupiter would still be bigger. Put another way, you could fit 11 Earths along Jupiter’s equator.",

         "Jupiter’s signature stripey look is actually made of clouds of ammonia and water. Its Great Red Spot is a huge elliptical storm system that’s lasted for hundreds of years, but may turn circular and dissolve at some point in the future.",

         "The gas giant planet may not actually have a solid surface, like our planet does. If it does have a secret inner core, scientists believe it’s likely teeny — about the size of one little Earth.",

         "If Venus has the longest day, Jupiter has the shortest in the solar system. It makes a full rotation once every dizzying 10 hours.",

         "Saturn is known for its seven large rings. The rings are made from ice, rocks, and dust, ranging from grain size to house size. Over time, the rings will break apart and eventually disappear, as the particles collide and get sent farther out into space or fall down to the planet.",

         "The least dense of all the planets, Saturn would float in water if you could find a way to set it in a ginormous swimming pool.",

         "Saturn leads the solar system moon count with 82 moons!",

         "Ancient astronomers discovered Saturn because it can be seen with the naked eye. It’s the farthest planet we can easily see without a telescope.",

         "Saturn has a hexagonal-shaped jet stream located at its north pole, containing massive storms and cloud formations. This unique weather system has yet to be found anywhere else in our solar system!",

         "Uranus appears blue-green in color due to the methane gas in its atmosphere. (Cue the fart jokes.) ",

         "The planet is quite contrary, rotating not only east to west like Venus, but also on its side.",

         "Uranus was the first planet to be discovered via telescope. You can technically see it with the naked eye, but it’s very difficult.",

         "While other planets and moons are named for mythological characters, Uranus’s 27 moons (among them Titania, Oberon, Puck, and Juliet), take their names from the writings of William Shakespeare and Alexander Pope.",

         "Uranus has two sets of rings, totaling 13 rings in all, but they aren’t as visually striking as Saturn’s.",

         "Ice giant Neptune, like Uranus, looks blue because of the methane in its atmosphere. Its color is likely why it was named for the Roman god of the sea.",

         "Neptune is the only planet humans cannot see with the naked eye. In fact, its existence was predicted by mathematical equations before scientists found it with a telescope. (Remember that the next time you’re bored in math class!)",

         "Neptune and dwarf planet Pluto like to cross paths. Because Pluto’s orbit is elliptical, sometimes it’s closer to the sun and sometimes Neptune is. It takes 165 Earth years for Neptune to circle the sun. In fact, Neptune has only completed one orbit since it was discovered in 1846.",

         "The windiest planet in the solar system, Neptune can experience winds of more than 1,200 miles per hour. Compare that to Earth’s worst storms that top out at 250 miles per hour.",

         "Neptune has 14 moons. One of Neptune's moons, Triton, is unique in the solar system because it’s a large moon that orbits a planet in the opposite direction from the planet’s own rotation. (Other planets have small moons that do this, but not large ones.) This is called a retrograde orbit.",

         "Think Neptune is cold because it’s so far from the sun? You’re right — but scientists speculate that there could be an extremely hot ocean beneath the planet’s cold atmosphere.",
    )
}