package com.example.doyouknow.facts

object countries {
    val countryFactsTextList = listOf(
        "Canada may have the highest number of lakes in the world. Approximately 60% of the world's lakes are found in Canada.",

        "Niger has the world’s youngest population. Half of Niger’s population is 14 or younger, and the median age is 15.",

        "Saudi Arabia is the world's largest country without rivers. A river is a permanent body of running water.",

        "Greenland is the least densely populated country, with less than 0.2 people per square km, followed by Mongolia, Namibia, Australia, and Iceland.",

        "With an average around 1.8 meters above sea level, Maldives is the lowest on Earth, Its the most likely to be submerged underwater if sea levels rise due to climate change.",

        "Papua New Guinea is the most linguistically diverse nation with over 840 languages spoken.",

        "With the lowest birth rates and the highest death rates the world, Bulgaria population is projected to drop from 6.9 million in 2020 to 5.4 million in 2050, a 22.5% decline.",

        "As of June 2020, the United States had the highest number of incarcerated individuals worldwide, with more than 2.12 million people in prison.",

        "Bhutan is the only country in the world that measures Gross Domestic Happiness (GDH) instead of GDP.",

        "If you count everything, including overseas territories, then France claims the title by covering 12 time zones. The United States would be the runner-up with 11 and then Russia with 9.",

        "Canada is the most educated country in the world, with around 56.27 per cent of adults having earned some kind of higher education.",

        "Guam island is almost entirely coral and uses it to create its asphalt instead of importing sand.",

        "One of the official anthems of the micro-nation of Ladonia is the sound of a stone thrown into water.",

        "At 1896 km, Canada's Yonge street is the longest street in the world. ",

        "New Zealand was the first self-governing nation to give women the right to vote in 1893 - a move that was followed two years later by its neighbour australia.",

        "With enormous expanses of forest, Russia produces the most oxygen for the planet. ",

        "South Sudan is the most recent country to declare independence, which happened on july 9, 2011. ",

        "Switzerland consumes the most chocolate per year with approximately 10 kilos a year per person.",

        "If you count overseas territories, then it is actually France that covers the most time zones with a whopping 12. ",

        "The US spends almost three times more on healthcare than any other country in the world, but ranks last in life expectancy among the 12 wealthiest industrialized countries.",
    )

}