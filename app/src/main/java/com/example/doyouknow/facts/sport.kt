package com.example.doyouknow.facts

object sport {
    val sportFactsTextList = listOf(
        "The world record for the most consecutive push-ups is 10, 507. It was set by Minoru Yoshida from Japan in 1980.",

        "According to the World's Sports Encyclopedia, there are over 8000 sports!",

        "he first sport as we know it today was most likely wrestling, and it originated in Greece in 776BC.",

        "The diameter of a basketball hoop was once almost double the size of a basketball, meaning that two balls could fit in the net side by side! However, this was changed in 2015 and the hoop is now much smaller.",

        "Modern swimsuits have become so advanced that some of them move faster in water than human skin. Some people think that the more a swimsuit covers your body, the greater your advantage when swimming in the water.",

        "Sheffield FC is the world's oldest football club, founded in 1857.",

        "Tennis strings were traditionally made from the intestines of goats, cows, or sheep. Fibres were extracted from the intestines which contain collagen, responsible for the strings' elasticity. Nowadays they are mostly made from nylon.",

        "Olympic Gold Medals are made, mostly, of sterling silver, not gold. In fact, they haven't been made of pure gold since 1912!",

        "Golf balls have around 336 dimples. That's the average, but there's no limit to the number of dimples. There can be between 300 and 500. Their purpose is to make them travel further in the air.",

        "Ski ballet was once a competitive sport. It was even a demonstration sport in the 1988 and 1922 Winter Olympic games.",

        "Major League Baseball umpires are obliged to wear black underwear during games, in case their trousers split!",

        "The average lifespan of a Minor League Baseball ball is five to seven pitches.",

        "The phrase about winning 'hands down' was initially in reference to a jockey, who would win a race without pulling the reins of his horse or whipping his horse.",

        "The average distance a person walks in their lifetime is four times around the whole world.",

        "There are only eighteen minutes of action in the average baseball game.",

        "The grass at Wimbledon used to be around 5cm long until an English player was bitten by a snake in 1949. Now, it's 8mm long.",

        "Sports have been played in outer space - yes, a golf ball was hit by Alan Shephard, and a javelin thrown by Edgar Mitchell on the moon, in 1971.",

        "In the 1928 Olympics, Australian rower Bobby Pearce won his race against eight others, even though he stopped to let ducks pass him during the race.",

        "It is customary for jockeys to be paid in coins, no matter how much they win!",

        "The Stanley cup was originally two storeys tall before people realised it was a bit too difficult to transport.",

        "The fastest hat trick in National Hockey League history took 21 seconds. Bill Mosienko set the record in 1952.",

        "The most points ever scored in a basketball player's career is 38,387, by Kareem Abdul-Jabbar of the United States.",

        "Ben Smith holds the world record for the most consecutive marathons, having run 401 marathons in 401 days, covering 10,506 miles altogether.",

        "The longest boxing match in history lasted 110 rounds and went on for over seven hours. It was played in 1893 by Andy Bowen and Jack Burke.",

        "The longest tennis match lasted eleven hours and 5 minutes. It was played by John Isner of the USA and Nicolas Mahut from France.",

        "The world record for the longest bow and arrow shot us held by Matt Stutzman from the USA, with a distance of 283.464 metres.",

        "Rocky Marciano holds the career heavyweight boxing record, with 49 victories, 43 of them being knockouts. This record was set between 1948 and 1956, without any losses.",

        "The colours of the Olympic rings represent the flags of every country competing at the Olympics in 1912, the year that they were designed. They were designed by Frenchman and founder of the International Olympic Committee, Baron Pierre de Coubertin.",

        "Martina Navratilova has won six Wimbledon titles in a row. Unbeaten in this streak, she set the record between 1982 and 1987.",

        "Byron Nelson has won more Professional Golf Association tours back-to-back than any other professional golfer. In 1945, he won 11.",

        "In the year 1959, Pelé scored 127 goals, more than any other football player in a single calendar year.",

        "The USA's Connecticut Women's basketball team once won 90 games in a row. Not a single game was lost between the start of the 2008-2009 season and the end of 2010.",

        "The Professional Golfer's Association record for the highest score on a Par-4 is 16, and was set in 2011 by Kevin Na, a Korean American professional golfer.",

        "The Professional Golfer's Association record for the highest score on a Par-4 is 16, and was set in 2011 by Kevin Na, a Korean American professional golfer.",

        "Bowling was invented around 3200BC in Egypt.",

        "The oldest sports in the world are thought to be wrestling, running, javelin throwing, polo and hockey.",

        "The most dangerous sports in the world are thought to be running, scuba diving, canoeing, Grand Prix racing, and extreme mountain climbing.",
    )

}