package com.example.doyouknow.facts

object history {
    val historyFactsTextList = listOf(
        "During World War II, a Great Dane named Juliana was awarded the Blue Cross Medal. She extinguished an incendiary bomb by peeing on it!",
        
        "Alexander the Great was accidentally buried alive. Scientists believe Alexander suffered from a neurological disorder called Guillain-Barré Syndrome. They believe that when he died he was actually just paralyzed and mentally aware!",
        
        "There were female Gladiators in Ancient Rome! A female gladiator was called a Gladiatrix, or Gladiatrices. They were extremely rare, unlike their male counterparts.",
        
        "The world’s most successful pirate in history was a lady named Ching Shih. She was a prostitute in China until the Commander of the Red Flag Fleet bought and married her. But, her husband considered her his equal and she became an active pirate commander in the fleet.",
        
        "You may know them as the bunch of heroes that broke box office records with their movies. But, The Avengers was also a group of Jewish assassins who hunted Nazi war criminals after World War II. They poisoned 2,283 German prisoners of war!",
        
        "From 1912 to 1948, the Olympic Games held competitions in the fine arts. Medals were given for literature, architecture, sculpture, painting, and music. Obviously, the art created was required to be Olympic-themed.",
        
        "Famous conqueror, Napoleon Bonaparte, was once attacked by a horde of bunnies! He had requested that a rabbit hunt be arranged for himself and his men. When the rabbits were released from their cages, the bunnies charged toward Bonaparte and his men in an unstoppable onslaught.",
        
        "Cleopatra wasn’t actually Egyptian! As far as historians can tell, Egypt’s famous femme fatal was actually Greek!. She was a descendant of Alexander the Great’s Macedonian general Ptolemy.",
        
        "Did you know Abraham Lincoln is in the wrestling hall of fame? The 6’4″ president had only one loss among his around 300 contests. He earned a reputation for this in New Salem, Illinois, as an elite fighter.",
        
        "George Washington opened a whiskey distillery after his presidency. After his term, Washington opened a whiskey distillery. By 1799, Washington’s distillery was the largest in the country, producing 11,000 gallons of un-aged whiskey!",
        
        "During the Salem witch trials, the accused witches weren’t actually burned at the stake. The majority were jailed, and some were hanged. But none of the 2,000 people accused ever got burned alive.",
        
        "President Zachary Taylor died from a cherry overdose! Zachary Taylor passed away after eating way too many cherries and drinking milk at a Fourth of July party in 1850. He died on July 9th from gastroenteritis. The acid in cherries along with the milk is believed to have caused this.",
        
        "In the Ancient Olympics, athletes performed naked! This was to achieve closeness to the gods and also help detox their skin through sweating. In fact, the word “gymnastics” comes from the Ancient Greek words “gumnasía” (“athletic training, exercise”) and “gumnós” (“naked”).",
        
        "In 1386, a pig was executed in France. In the Middle Ages, a pig attacked a child who went to die later from their wounds. The pig was arrested, kept in prison, and then sent to court where it stood trial for murder, was found guilty and then executed by hanging!",
        
        "During the Great Depression, people made clothes out of food sacks. People used flour bags, potato sacks, and anything made out of burlap. Because of this trend, food distributors started to make their sacks more colorful to help people remain a little bit fashionable.",
        
        "During the Victorian period, it was normal to photograph loved ones after they died. People would dress their newly-deceased relatives in their best clothing, and then put them in lifelike poses and photograph them. They did this to preserve one last image of their dead loved one!",
        
        "The shortest war in history lasted 38 minutes! It was between Britain and Zanzibar and known as the Anglo-Zanzibar War, this war occurred on August 27, 1896. It was over the ascension of the next Sultan in Zanzibar and resulted in a British victory.",
        
        "The University of Oxford is older than the Aztec Empire. The University of Oxford first opened its doors to students all the way back in 1096. By comparison, the Aztec Empire is said to have originated with the founding of the city of Tenochtitlán at Lake Texcoco by the Mexica which occurred in the year 1325.",
        
        "Russia ran out of vodka celebrating the end of World War II! When the long war ended, street parties engulfed the Soviet Union, lasting for days–until all of the nation’s vodka reserves ran out a mere 22 hours after the partying started.",
        
        "The first official Medals of Honor were awarded during the American Civil War. They were awarded to Union soldiers who participated in the Great Locomotive Chase of 1862.",
        
        "In 18th century England, pineapples were a status symbol. Those rich enough to own a pineapple would carry them around to signify their personal wealth and high-class status. In that day and age, everything from clothing to houseware was decorated with the tropical fruit.",
        
        "In Ancient Greece, they believed redheads became vampires after death! This was partly because redheaded people are very pale-skinned and sensitive to sunlight. Unlike the Mediterranean Greeks who had olive skin and dark features.",
        
        "Tablecloths were originally designed to be used as one big, communal napkin. When they were first invented, guests were meant to wipe off their hands and faces on a tablecloth after a messy dinner party.",
        
        "For over 30 years, Canada and Denmark have been playfully fighting for control of a tiny island near Greenland called Hans Island. Once in a while, when officials from each country visit, they leave a bottle of their country’s liquor as a power move.",
        
        "In 1945, a balloon bomb launched by Japan landed in Oregon. It fell upon by a woman and five children, who died when it exploded. These were the only World War II casualties on US soil.",
        
        "Roman gladiators often became celebrities and even endorsed products! Children would even play with gladiator ‘action figures’ made out of clay. Also, their sweat was considered an aphrodisiac and women would mix it into their skincare products",
        
        "Cars weren’t invented in the United States! The first car actually was created in the 19th century when European engineers Karl Benz and Emile Levassor were working on automobile inventions. Benz patented the first automobile in 1886.",
        
        "Roman Catholics in Bavaria founded a secret society in 1740 called the Order of the Pug. New members had to wear dog collars and scratch at the door to get in. This para-Masonic society was reportedly active until 1902!",
        
        "The ancient Romans often used stale urine as mouthwash. The main ingredient in urine is ammonia which acts as a powerful cleaning agent. Urine became so in demand that Romans who traded in it actually had to pay a tax!",
        
        "In Ancient Asia, death by elephant was a popular form of execution. They could be taught to slowly break bones, crush skulls, twist off limbs, or even execute people using large blades fitted to their tusks. In some parts of Asia, this method of execution was still popular up to the late 19th century.",
        
        "Back in the 16th century, the wealthy elite used to eat dead bodies. It was rumored the cadavers could cure diseases. The highest delicacy? Egyptian mummies.",
        
        "Before the 19th century, dentures were made from dead soldiers’ teeth. After the Battle of Waterloo, dentists ran to the battlefield to seek out teeth from the thousands of dead soldiers. They then took their bounty to their dental practices and crafted them into dentures for the toothless elite.",
        
        "In 1644, English statesman, Oliver Cromwell, banned the eating of pie. He declared it a pagan form of pleasure. For 16 years, pie eating and making went underground until the Restoration leaders lifted the ban on pie in 1660.",
        
        " The South African railway once employed a baboon. In his right years of service, he never made a single mistake.",
        
        "It’s believed that roughly 97% of history has been lost over time. Written accounts of history only started roughly 6,000 years ago. And modern humans first appeared around 200,000 years ago.",
    )
}