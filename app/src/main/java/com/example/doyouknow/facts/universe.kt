package com.example.doyouknow.facts

object universe {
    val universeFactsTextList = listOf(
        "Almost all of ordinary matter (99.9999999% of it) is empty space. If you took out all of the space in our atoms, the entire human race (all 7 billion of us) would fit into the volume of a sugar cube.",

        "Many of the atoms you're made of, from the calcium in your bones to the iron in your blood, were brewed up in the heart of an exploding star billions of years ago.",

        "In fact, your body contains cosmic relics from the creation of the universe. Almost all of your hydrogen atoms were formed in the Big Bang, about 13.7 billion years ago.",

        "And back in the days before digital television, if you tuned your TV between stations, a small percentage of the static you would see would actually be the afterglow of the Big Bang.",

        "Light from some stars takes so long to travel to our eyes that when you look at the star-speckled night sky you're actually peering deep into the past. NASA's Hubble Telescope can look as far back as 13 billion years ago.",

        "Forty-seven years ago, humankind took its first steps on the moon — and the bootprints will probably still be there a million years from now. That's because the moon has no atmosphere, so there's no wind or water to sweep through and erase the marks.",

        "Outer space is silent. Eerily silent. That's because sound waves need some sort of medium to travel through. And space is a vacuum. A dark, silent vacuum.",

        "If you touch two pieces of the same type of metal together in the vacuum of space, they will fuse, bound together until eternity (or until you break them apart).",

        "One year on Venus is equal to 224 Earth days. And one day on Venus is equal to 243 Earth days. Which makes a day on Venus longer than a year. And to top it off, Venus is the only planet in our solar system that rotates backwards.",

        "The sun makes up 99.86% of the mass of the solar system. It's so big that you could squeeze 1.3 million Earths inside of it.",

        "There might be as many as three sextillion stars in the universe. That's 3 followed by 23 zeros, or 300,000,000,000,000,000,000,000. That's more than all of the grains of sand on Earth.",

        "When a massive star explodes, its scrunched up core forms something called a neutron star. Neutron stars are so dense that just a teaspoon of their material would weigh more than Mt. Everest. The explosion can spin the neutron star to mind blowing speeds — up to 600 rotations per second.",

        "Ordinary, observable matter (like stars and planets) makes up a measly 5% of the universe. The other 95% universe is made up of invisible dark energy (68%) and dark matter (27%). That means there's 95% of the universe that we don't know about yet.",

        "It is impossible to predict just how many stars are there in our universe. Right now, as per the scientists and astronomers, there is an estimate of 200 to 400 billion stars in the milky way itself. With this, there are billions of galaxies in our universe.",

        "After 164BC there was a continuous recording of the comet each time it was visible. The next Halley's comet will be seen from earth in the year 2061.",

        "The cost of the NASA space suit costs for around \$12,000,000. However, 70 percent of the total cost is for the backpack and control module.",

        "Astronomers have found a massive water vapor cloud which holds 140 trillion times the mass of water in the Earth's oceans somewhere around 10 billion light years away. It is the largest discovery of water ever found.",

        "Every year the moon is drifting away from Earth by 3.8 cm. Scientists do believe that eventually, the Moon will move out of the field of Earth's Gravity. However, this won't happen for the next billions of years to come.",

        "The first ever black hole photo which was released in April 2019 shows a halo of dust and gas. It is stated to be 310 million trillion miles from earth. The photo was captured by the Event Horizon Telescope, a network of eight linked telescopes, and was also captured due to the algorithm of programmer Katie Bowman.",

        "The Sun accounts for 99.86% of the mass in our solar system with a mass of around 330,000 times that of Earth. Did you know that the Sun is made up of mostly hydrogen (three quarters worth) with the rest of its mass attributed to helium.  If the Sun had a voice would it be high and squeaky from all that helium?",

        "The universe extends far beyond our own galaxy, The Milky Way, which is why scientists can only estimate how many stars are in space.  However, scientists estimate the universe contains approximately 1,000,000,000,000,000,000,000,000 stars, or a septillion.   While no one can actually count every single grain of sand on the earth, the estimated total from researchers at the University of Hawaii, is somewhere around seven quintillion, five hundred quadrillion grains.  That is an awfully big sand castle!",

        "There’s a planet made of diamonds twice the size of earth The \"super earth,\" aka 55 Cancri e, is most likely covered in graphite and diamond.  Paying a visit to that planet would probably pay for the \$12 million dollar space suit needed to get there!",

        "In the beginning, the universe was infinitely hot – minutes after its birth estimates suggest that it had around 1 billion Kelvin degrees.",

        "As the universe grew old, it also became much colder. Current average temperatures are around 2.725 degrees, Kelvin.",

        "The exact size of the universe is impossible to calculate. However, the observable universe is around 93 billion light-years with a radius of around 46.5 billion light-years.",

        "The universe is made up of 4.9% ordinary (baryonic) matter, 26.8% dark matter, and 68.3% dark energy. Considering only the largest structures, the universe is made up of filaments, voids, superclusters, and galaxy groups, and clusters.",

        "Dark matter and dark energy are invisible theoretical matter. The only proof of their existence lies in certain phenomena which mostly tell us that huge chunks of mass are missing, thus, in a way, this justifies their probable existence.",

        "The age of the universe is predicted to be around 13.8 billion years old.",

        "The universe is not only expanding, but it is also accelerating its expansion. Galaxies move away from each other but at the same time, space is also moving/expanding.",
    )
}