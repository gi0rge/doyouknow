package com.example.doyouknow.facts

object human {
    val humanFactsTextList = listOf(
        "If you unraveled all of the DNA in your body, it would span 34 billion miles, reaching to Pluto (2.66 billion miles away) and back ... six times.",

        "Infants are born with approximately 300 bones, but as they grow some of these bones fuse together. By the time they reach adulthood, they only have 206 bones.",

        "Every second, your body produces 25 million new cells. That means in 15 seconds, you will have produced more cells than there are people in the United States.",

        "There is anywhere between 60,000-100,000 miles of blood vessels in the human body. If they were taken out and laid end-to-end, they would be long enough to travel around the world more than three times.",

        "Teeth are considered part of the skeletal system, but are not counted as bones.",

        "Your left and right lungs aren’t exactly the same. The lung on the left side of your body is divided into two lobes while the lung on your right side is divided into three. The left lung is also slightly smaller, allowing room for your heart.",

        "The brain uses over a quarter of the oxygen used by the human body.",

        "Your heart beats around 100,000 times a day, 365,00,000 times a year and over a billion times if you live beyond 30.",

        "Grouping human blood types can be a difficult process and there are currently around 30 recognised blood types (or blood groups). You might be familiar with the more simplified “ABO” system which categorises blood types under O, A, B and AB.",

        "The outer layer of your skin is the epidermis, it is found thickest on the palms of your hands and soles of your feet (around 1.5 mm thick).",

        "Humans have a stage of sleep that features rapid eye movement (REM). REM sleep makes up around 25 per cent of total sleep time and is often when you have your most vivid dreams.",

        "The smallest bone found in the human body is located in the middle ear. The staples (or stirrup) bone is only 2.8 millimetres long.",

        "Goose bumps evolved to make our ancestors’ hair stand up, making them appear more threatening to predators.",

        "The cornea is the only part of the body with no blood supply – it gets its oxygen directly from the air.",

        "An average sized man eats about 33 tons of food in his/her life time which is about the weight of six elephants.",

        "Nephrons, the kidney’s filtering units, clean the blood in the human body in about 45 minutes and send about six cups of urine (2000 ml) to the bladder every day.",

        "The average person produces enough saliva in their lifetime to fill two swimming pools.",

        "There are about ten thousand taste buds on the human tongue and in general girls have more taste buds than boys!",

        "The left side of your brain controls the right side of your body and right side of your brain controls the left side of your body.",

        "Our brain is programmed to erect the inverted image formed on our retina by the convex eye lens. A newborn baby sees the world upside down till its brain starts erecting it.",

        "You carry, on average, about four pounds of bacteria around in your body.",

        "Sometimes the pain from scratching makes your body release the pain-fighting chemical serotonin. It can make the itch feel even itchier.",

        "A running nose is the way our body flushes out germs from our nose while we catch cold and flu.",

        "On average, human body contains enough iron to make a nail 2.5 cm (1 inch) long.",
    )
}