package com.example.doyouknow.facts

object health {
    val healthFactsTextList = listOf(
        "Laughing is good for the heart and can increase blood flow by 20 percent.",

        "Your skin works hard. Not only is it the largest organ in the body, but it regulates your temperature and defends against disease and infection.",

        "Always look on the bright side: being an optimist can help you live longer.",

        "Exercise will give you more energy, even when you’re tired.",

        "Sitting and sleeping are great in moderation, but too much can increase your chances of an early death.",

        "Feeling stressed? Read. Getting lost in a book can lower levels of cortisol, and other unhealthy stress hormones, by 68%.",

        "Maintaining good relationships with your friends and family, reduces harmful levels of stress and boosts your immune system.",

        "Smelling rosemary may increase alertness and improve memory so catch a whiff before a test or important meeting.",

        "Swearing can make you feel better when you’re in pain.",

        "Chocolate is good for your skin; its antioxidants improve blood flow and protect against UV damage.",

        "Tea can lower risks of heart attack, certain cancers, type 2 Diabetes and Parkinson’s disease. Just make sure your tea isn’t too sweet!",

        "Although it only takes you a few minutes to eat a meal, it takes your body hours to completely digest the food.",

        "An apple a day does keep the doctor away. Apples can reduce levels of bad cholesterol to keep your heart healthy.",

        "Vitamin D is as important as calcium in determining bone health, and most people don’t get enough of it.",

        "Walking at a fast pace for three hours or more at least once a week, you can reduce your risk of heart disease by up to 65%.",

        "Drinking at least five glasses of water a day can reduce your chances of suffering from a heart attack by 40%.",

        "Dehydration can have a negative impact on your mood and energy levels. Drink enough water to ensure you’re always at your best.",

        "Massage isn’t just for the muscles. It can help scars fade, and can be more beneficial than lotion or oil.",

        "Brushing teeth too soon after eating or drinking can soften the tooth enamel, especially if you’ve just been eating or drinking acidic foods.",

        "The blue light from your phone can mess with your circadian rhythm.",

        "Wild-caught fish, grass-fed meats and free-range eggs are simple ways to inject healthy changes to your diet without drastically altering what you eat.",

        "Breathing deeply in moments of stress, or anytime during the day, brings many benefits such as better circulation, decreased anxiety and reduced blood pressure.",

        "‘Gymnasium’ comes from the Greek word ‘gymnazein’, meaning ‘to exercise naked’.",

        "On average, there are more bacteria per square inch in a kitchen sink than the bathroom.",

        "During an allergic reaction your immune system is responding to a false alarm that it perceives as a threat.",

        "The eye muscles are the most active in the body, moving more than 100,000 times a day!",

        "Humans can cough at 60 miles an hour and sneezes can be 100 miles an hour – which is faster than the average car!",

        "The soles of your feet contain more sweat glands and nerve endings per square inch than anywhere else on your body.",
        )
}