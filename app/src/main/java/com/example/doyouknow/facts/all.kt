package com.example.doyouknow.facts

object all {
    val allFactsTextList = listOf(
        "To the untrained eye, jaguars can be mistaken for leopards, but you can tell the difference from their rosettes (circular markings): Jaguars have black dots in the middle of some of their rosettes, whereas leopards don’t. Jaguars also have larger, rounded heads and short legs. ",

        "Sumatran rhinos are the smallest, but they can still weigh 600kg (that’s almost 95 stone). And white rhinos are the largest, weighing up to 3,500kg (over 550 stone, or well over 3 tonnes!).",

        "Orangutans are incredibly dexterous and use both hands and feet while gathering food and travelling through the trees.",

        "Sometimes, to mark their scent, panda's climb a tree backwards with their hindfeet until they're in a full handstand upside down - enabling them to leave their scent higher up. ",

        "Tigers' main prey is deer but they also eat wild boar. For tigers, only one in ten hunts are successful, a large deer can provide a tiger with one week's food.",

        "The African elephant is the world's largest land mammal – with males on average measuring up to 3m high and weighing up to 6 tonnes.",

        "Elephants have around 150,000 muscle units in their trunk. Their trunks are perhaps the most sensitive organ found in any mammal - Asian elephants have been seen to pick up a peanut, shell it, blow the shell out and eat the nut.",

        "Nearly all wild lions live in Africa, below the Sahara Desert, but one small population exists around Gir Forest National Park in western India.",

        "This means that exposure to human illnesses – even a cold - can have potentially detrimental impacts on gorillas as they are so genetically similar to us, but they haven't developed the necessary immunities.",

        "We share around 98% of our DNA with gorillas. This means that exposure to human illnesses – even a cold - can have potentially detrimental impacts on gorillas as they are so genetically similar to us, but they haven't developed the necessary immunities. ",

        "Turtles don't have teeth. They use their beak-like mouth to grasp their food. This beak is made of keratin (the same stuff your fingernails are made of).",

        "Sharks live in most ocean habitats. They can be found in beautiful, tropical coral reefs, to the deep sea, and even under the arctic sea ice.",

        "Bottlenose dolphins are one of the few species, along with apes and humans, that have the ability to recognise themselves in a mirror. This is considered 'reflective' of their intelligence. Dolphins are also among the few animals that have been documented using tools. In Shark Bay in Western Australia, dolphins fit marine sponges over their beaks to protect them from sharp, harmful rocks as they forage for fish.",

        "Humpback whales in the Southern Hemisphere live off their fat reserves for 5.5-7.5 months each year, as they migrate from their tropical breeding grounds to the Antarctic, to feed on krill.",

        "Whale sharks can grow up to 12 metres long. But despite their size, whale sharks are often referred to as \"gentle giants\".",

        "Emperors are the biggest of the 18 species of penguin found today, and one of the largest of all birds. They are approximately 120cm tall (about the height of a six year old child) and weigh in at around 40 kg, though their weight does fluctuate dramatically throughout the year.",

        "Polar bear fur is translucent, and only appears white because it reflects visible light. Beneath all that thick fur, their skin is jet black.",

        "Unlike other big cats, snow leopards can't roar. Snow leopards have a 'main' call described as a 'piercing yowl' that's so loud it can be heard over the roar of a river.",

        "Three weeks before they started recording The Walking Dead, Andrew Lincoln (who plays Rick Grimes) went out and practiced his accent by ordering coffees & fried chicken!",

        "Bruno Mars‘ real name is Peter Gene Hernandez.",

        "Eminem’s song “Lose Yourself” was the first rap song to win an Oscar for Best Original Song, however, instead of watching the awards, Eminem was asleep watching cartoons with his daughter.",

        "Bryan Cranston’s parents didn’t want him to have a career in Hollywood, so he didn’t start pursuing his acting career until he left college. You may also know him as Walter White from the hit television show Breaking Bad.",

        "Robert Downey Jr. claims that Burger King saved his life from his drug addiction.",

        "In March 2006, Beyoncé accepted a star on the Hollywood Walk of Fame.",

        "Nathan Sykes from the British boy-band “The Wanted” used to have a rabbit called Pikachu when he was younger.",

        "On August 16, 2005, Madonna fell off a horse at her home outside London; she suffered from 3 cracked ribs, a broken collarbone, and a broken hand.",

        "Brad Pitt first gained recognition as a cowboy hitchhiker in the road movie Thelma & Louise.",

        "Angelina Jolie performed her own stunts whilst filming Lara Croft: Tomb Raider.",

        "For the movie “The Scorpion King,” Dwayne Johnson earned \$5.5 million and registered his name in the Guinness World Records for receiving the highest salary for a first-time leading role.",

        "Uncle Phil from Fresh Prince of Bel-Air was also the voice of Shredder from the original Teenage Mutant Ninja Turtles cartoon.",

        "Although Emma Watson is British, she was actually born in Paris, France on April 15, 1990.",

        "In 2011, Jennifer Lawrence appeared in The Beaver with Mel Gibson, Jodie Foster, and Anton Yelchin, and played Mystique in X-Men: First Class.",

        "Orlando Bloom learned how to surf in New Zealand whilst filming “The Lord of the Rings.”",

        "Tobey Maguire, who played Spider-Man in the 2002 film, had a fear of heights.",

        "Taylor Swift is extremely talented, she can play the guitar, piano, ukulele, electric guitar, and the banjo!",

        "George Lucas originally wanted to be a race car driver, but an accident just before his high school graduation changed his mind.",

        "Leonardo DiCaprio has never done drugs and had to speak to a drug expert to prepare for Wolf of Wall Street.",

        "Jennifer Lopez was the first actress to have a movie and an album hit number one in the same week.",

        "At age 15, Cristiano Ronaldo was diagnosed with a heart condition that required surgery, he was briefly sidelined and made a full recovery.",

        "Ryan Gosling was asked to audition for boy-band Backstreet Boys but he turned it down.",

        "Ben Affleck starred in, directed, and produced the movie “Argo” in 2012, which earned him awards for acting, directing, producing and writing.",

        "Before becoming a movie star, Harrison Ford was a master carpenter – a craft he still does as a hobby.",

        "Miley Cyrus first tried out for Hannah Montana at the age of 11. She was denied due to her age.",

        "As a child, Jim Carrey wore tap shoes to bed just in case his parents needed cheering up in the middle of the night.",

        "Megan Fox was banned from the Wal-Mart store in her hometown because she shoplifted makeup as a teenager. She claims it was an accident.",

        "Eminem originally wanted to become a comic-book artist.",

        "Evangeline Lilly is a notoriously bad driver. She has been in at least 8 car accidents, and none of them were with another car.",

        "Colin Farrell says that Marilyn Monroe was the first woman he fell in love with.",

        "Michael Jackson loved Mexican food; later in his life he loved KFC’s fried chicken.",

        "Elvis Presley’s daughter, Lisa Marie, became the wife of Michael Jackson on 26th May 1994, but they divorced after 4 years.",

        "Kanye West’s song “Hey Mama” was dedicated to his mother, who died in 2007 after complications with plastic surgery.",

        "Willow Smith’s musical debut is 8 years earlier than her fathers (Will Smith), making her the youngest debuting superstar in her family.",

        "Cameron Diaz had no previous acting experience before she got the part in The Mask.",

        "Daniel Radcliffe broke over 80 wands while filming the Harry Potter movies because he used them as drumsticks!",

        "Canada may have the highest number of lakes in the world. Approximately 60% of the world's lakes are found in Canada.",

        "Niger has the world’s youngest population. Half of Niger’s population is 14 or younger, and the median age is 15.",

        "Saudi Arabia is the world's largest country without rivers. A river is a permanent body of running water.",

        "Greenland is the least densely populated country, with less than 0.2 people per square km, followed by Mongolia, Namibia, Australia, and Iceland.",

        "With an average around 1.8 meters above sea level, Maldives is the lowest on Earth, Its the most likely to be submerged underwater if sea levels rise due to climate change.",

        "Papua New Guinea is the most linguistically diverse nation with over 840 languages spoken.",

        "With the lowest birth rates and the highest death rates the world, Bulgaria population is projected to drop from 6.9 million in 2020 to 5.4 million in 2050, a 22.5% decline.",

        "As of June 2020, the United States had the highest number of incarcerated individuals worldwide, with more than 2.12 million people in prison.",

        "Bhutan is the only country in the world that measures Gross Domestic Happiness (GDH) instead of GDP.",

        "If you count everything, including overseas territories, then France claims the title by covering 12 time zones. The United States would be the runner-up with 11 and then Russia with 9.",

        "Canada is the most educated country in the world, with around 56.27 per cent of adults having earned some kind of higher education.",

        "Guam island is almost entirely coral and uses it to create its asphalt instead of importing sand.",

        "One of the official anthems of the micro-nation of Ladonia is the sound of a stone thrown into water.",

        "At 1896 km, Canada's Yonge street is the longest street in the world. ",

        "New Zealand was the first self-governing nation to give women the right to vote in 1893 - a move that was followed two years later by its neighbour australia.",

        "With enormous expanses of forest, Russia produces the most oxygen for the planet. ",

        "South Sudan is the most recent country to declare independence, which happened on july 9, 2011. ",

        "Switzerland consumes the most chocolate per year with approximately 10 kilos a year per person.",

        "If you count overseas territories, then it is actually France that covers the most time zones with a whopping 12. ",

        "The US spends almost three times more on healthcare than any other country in the world, but ranks last in life expectancy among the 12 wealthiest industrialized countries.",

        "Laughing is good for the heart and can increase blood flow by 20 percent.",

        "Your skin works hard. Not only is it the largest organ in the body, but it regulates your temperature and defends against disease and infection.",

        "Always look on the bright side: being an optimist can help you live longer.",

        "Exercise will give you more energy, even when you’re tired.",

        "Sitting and sleeping are great in moderation, but too much can increase your chances of an early death.",

        "Feeling stressed? Read. Getting lost in a book can lower levels of cortisol, and other unhealthy stress hormones, by 68%.",

        "Maintaining good relationships with your friends and family, reduces harmful levels of stress and boosts your immune system.",

        "Smelling rosemary may increase alertness and improve memory so catch a whiff before a test or important meeting.",

        "Swearing can make you feel better when you’re in pain.",

        "Chocolate is good for your skin; its antioxidants improve blood flow and protect against UV damage.",

        "Tea can lower risks of heart attack, certain cancers, type 2 Diabetes and Parkinson’s disease. Just make sure your tea isn’t too sweet!",

        "Although it only takes you a few minutes to eat a meal, it takes your body hours to completely digest the food.",

        "An apple a day does keep the doctor away. Apples can reduce levels of bad cholesterol to keep your heart healthy.",

        "Vitamin D is as important as calcium in determining bone health, and most people don’t get enough of it.",

        "Walking at a fast pace for three hours or more at least once a week, you can reduce your risk of heart disease by up to 65%.",

        "Drinking at least five glasses of water a day can reduce your chances of suffering from a heart attack by 40%.",

        "Dehydration can have a negative impact on your mood and energy levels. Drink enough water to ensure you’re always at your best.",

        "Massage isn’t just for the muscles. It can help scars fade, and can be more beneficial than lotion or oil.",

        "Brushing teeth too soon after eating or drinking can soften the tooth enamel, especially if you’ve just been eating or drinking acidic foods.",

        "The blue light from your phone can mess with your circadian rhythm.",

        "Wild-caught fish, grass-fed meats and free-range eggs are simple ways to inject healthy changes to your diet without drastically altering what you eat.",

        "Breathing deeply in moments of stress, or anytime during the day, brings many benefits such as better circulation, decreased anxiety and reduced blood pressure.",

        "‘Gymnasium’ comes from the Greek word ‘gymnazein’, meaning ‘to exercise naked’.",

        "On average, there are more bacteria per square inch in a kitchen sink than the bathroom.",

        "During an allergic reaction your immune system is responding to a false alarm that it perceives as a threat.",

        "The eye muscles are the most active in the body, moving more than 100,000 times a day!",

        "Humans can cough at 60 miles an hour and sneezes can be 100 miles an hour – which is faster than the average car!",

        "The soles of your feet contain more sweat glands and nerve endings per square inch than anywhere else on your body.",

        "During World War II, a Great Dane named Juliana was awarded the Blue Cross Medal. She extinguished an incendiary bomb by peeing on it!",

        "Alexander the Great was accidentally buried alive. Scientists believe Alexander suffered from a neurological disorder called Guillain-Barré Syndrome. They believe that when he died he was actually just paralyzed and mentally aware!",

        "There were female Gladiators in Ancient Rome! A female gladiator was called a Gladiatrix, or Gladiatrices. They were extremely rare, unlike their male counterparts.",

        "The world’s most successful pirate in history was a lady named Ching Shih. She was a prostitute in China until the Commander of the Red Flag Fleet bought and married her. But, her husband considered her his equal and she became an active pirate commander in the fleet.",

        "You may know them as the bunch of heroes that broke box office records with their movies. But, The Avengers was also a group of Jewish assassins who hunted Nazi war criminals after World War II. They poisoned 2,283 German prisoners of war!",

        "From 1912 to 1948, the Olympic Games held competitions in the fine arts. Medals were given for literature, architecture, sculpture, painting, and music. Obviously, the art created was required to be Olympic-themed.",

        "Famous conqueror, Napoleon Bonaparte, was once attacked by a horde of bunnies! He had requested that a rabbit hunt be arranged for himself and his men. When the rabbits were released from their cages, the bunnies charged toward Bonaparte and his men in an unstoppable onslaught.",

        "Cleopatra wasn’t actually Egyptian! As far as historians can tell, Egypt’s famous femme fatal was actually Greek!. She was a descendant of Alexander the Great’s Macedonian general Ptolemy.",

        "Did you know Abraham Lincoln is in the wrestling hall of fame? The 6’4″ president had only one loss among his around 300 contests. He earned a reputation for this in New Salem, Illinois, as an elite fighter.",

        "George Washington opened a whiskey distillery after his presidency. After his term, Washington opened a whiskey distillery. By 1799, Washington’s distillery was the largest in the country, producing 11,000 gallons of un-aged whiskey!",

        "During the Salem witch trials, the accused witches weren’t actually burned at the stake. The majority were jailed, and some were hanged. But none of the 2,000 people accused ever got burned alive.",

        "President Zachary Taylor died from a cherry overdose! Zachary Taylor passed away after eating way too many cherries and drinking milk at a Fourth of July party in 1850. He died on July 9th from gastroenteritis. The acid in cherries along with the milk is believed to have caused this.",

        "In the Ancient Olympics, athletes performed naked! This was to achieve closeness to the gods and also help detox their skin through sweating. In fact, the word “gymnastics” comes from the Ancient Greek words “gumnasía” (“athletic training, exercise”) and “gumnós” (“naked”).",

        "In 1386, a pig was executed in France. In the Middle Ages, a pig attacked a child who went to die later from their wounds. The pig was arrested, kept in prison, and then sent to court where it stood trial for murder, was found guilty and then executed by hanging!",

        "During the Great Depression, people made clothes out of food sacks. People used flour bags, potato sacks, and anything made out of burlap. Because of this trend, food distributors started to make their sacks more colorful to help people remain a little bit fashionable.",

        "During the Victorian period, it was normal to photograph loved ones after they died. People would dress their newly-deceased relatives in their best clothing, and then put them in lifelike poses and photograph them. They did this to preserve one last image of their dead loved one!",

        "The shortest war in history lasted 38 minutes! It was between Britain and Zanzibar and known as the Anglo-Zanzibar War, this war occurred on August 27, 1896. It was over the ascension of the next Sultan in Zanzibar and resulted in a British victory.",

        "The University of Oxford is older than the Aztec Empire. The University of Oxford first opened its doors to students all the way back in 1096. By comparison, the Aztec Empire is said to have originated with the founding of the city of Tenochtitlán at Lake Texcoco by the Mexica which occurred in the year 1325.",

        "Russia ran out of vodka celebrating the end of World War II! When the long war ended, street parties engulfed the Soviet Union, lasting for days–until all of the nation’s vodka reserves ran out a mere 22 hours after the partying started.",

        "The first official Medals of Honor were awarded during the American Civil War. They were awarded to Union soldiers who participated in the Great Locomotive Chase of 1862.",

        "In 18th century England, pineapples were a status symbol. Those rich enough to own a pineapple would carry them around to signify their personal wealth and high-class status. In that day and age, everything from clothing to houseware was decorated with the tropical fruit.",

        "In Ancient Greece, they believed redheads became vampires after death! This was partly because redheaded people are very pale-skinned and sensitive to sunlight. Unlike the Mediterranean Greeks who had olive skin and dark features.",

        "Tablecloths were originally designed to be used as one big, communal napkin. When they were first invented, guests were meant to wipe off their hands and faces on a tablecloth after a messy dinner party.",

        "For over 30 years, Canada and Denmark have been playfully fighting for control of a tiny island near Greenland called Hans Island. Once in a while, when officials from each country visit, they leave a bottle of their country’s liquor as a power move.",

        "In 1945, a balloon bomb launched by Japan landed in Oregon. It fell upon by a woman and five children, who died when it exploded. These were the only World War II casualties on US soil.",

        "Roman gladiators often became celebrities and even endorsed products! Children would even play with gladiator ‘action figures’ made out of clay. Also, their sweat was considered an aphrodisiac and women would mix it into their skincare products",

        "Cars weren’t invented in the United States! The first car actually was created in the 19th century when European engineers Karl Benz and Emile Levassor were working on automobile inventions. Benz patented the first automobile in 1886.",

        "Roman Catholics in Bavaria founded a secret society in 1740 called the Order of the Pug. New members had to wear dog collars and scratch at the door to get in. This para-Masonic society was reportedly active until 1902!",

        "The ancient Romans often used stale urine as mouthwash. The main ingredient in urine is ammonia which acts as a powerful cleaning agent. Urine became so in demand that Romans who traded in it actually had to pay a tax!",

        "In Ancient Asia, death by elephant was a popular form of execution. They could be taught to slowly break bones, crush skulls, twist off limbs, or even execute people using large blades fitted to their tusks. In some parts of Asia, this method of execution was still popular up to the late 19th century.",

        "Back in the 16th century, the wealthy elite used to eat dead bodies. It was rumored the cadavers could cure diseases. The highest delicacy? Egyptian mummies.",

        "Before the 19th century, dentures were made from dead soldiers’ teeth. After the Battle of Waterloo, dentists ran to the battlefield to seek out teeth from the thousands of dead soldiers. They then took their bounty to their dental practices and crafted them into dentures for the toothless elite.",

        "In 1644, English statesman, Oliver Cromwell, banned the eating of pie. He declared it a pagan form of pleasure. For 16 years, pie eating and making went underground until the Restoration leaders lifted the ban on pie in 1660.",

        " The South African railway once employed a baboon. In his right years of service, he never made a single mistake.",

        "It’s believed that roughly 97% of history has been lost over time. Written accounts of history only started roughly 6,000 years ago. And modern humans first appeared around 200,000 years ago.",

        "If you unraveled all of the DNA in your body, it would span 34 billion miles, reaching to Pluto (2.66 billion miles away) and back ... six times.",

        "Infants are born with approximately 300 bones, but as they grow some of these bones fuse together. By the time they reach adulthood, they only have 206 bones.",

        "Every second, your body produces 25 million new cells. That means in 15 seconds, you will have produced more cells than there are people in the United States.",

        "There is anywhere between 60,000-100,000 miles of blood vessels in the human body. If they were taken out and laid end-to-end, they would be long enough to travel around the world more than three times.",

        "Teeth are considered part of the skeletal system, but are not counted as bones.",

        "Your left and right lungs aren’t exactly the same. The lung on the left side of your body is divided into two lobes while the lung on your right side is divided into three. The left lung is also slightly smaller, allowing room for your heart.",

        "The brain uses over a quarter of the oxygen used by the human body.",

        "Your heart beats around 100,000 times a day, 365,00,000 times a year and over a billion times if you live beyond 30.",

        "Grouping human blood types can be a difficult process and there are currently around 30 recognised blood types (or blood groups). You might be familiar with the more simplified “ABO” system which categorises blood types under O, A, B and AB.",

        "The outer layer of your skin is the epidermis, it is found thickest on the palms of your hands and soles of your feet (around 1.5 mm thick).",

        "Humans have a stage of sleep that features rapid eye movement (REM). REM sleep makes up around 25 per cent of total sleep time and is often when you have your most vivid dreams.",

        "The smallest bone found in the human body is located in the middle ear. The staples (or stirrup) bone is only 2.8 millimetres long.",

        "Goose bumps evolved to make our ancestors’ hair stand up, making them appear more threatening to predators.",

        "The cornea is the only part of the body with no blood supply – it gets its oxygen directly from the air.",

        "An average sized man eats about 33 tons of food in his/her life time which is about the weight of six elephants.",

        "Nephrons, the kidney’s filtering units, clean the blood in the human body in about 45 minutes and send about six cups of urine (2000 ml) to the bladder every day.",

        "The average person produces enough saliva in their lifetime to fill two swimming pools.",

        "There are about ten thousand taste buds on the human tongue and in general girls have more taste buds than boys!",

        "The left side of your brain controls the right side of your body and right side of your brain controls the left side of your body.",

        "Our brain is programmed to erect the inverted image formed on our retina by the convex eye lens. A newborn baby sees the world upside down till its brain starts erecting it.",

        "You carry, on average, about four pounds of bacteria around in your body.",

        "Sometimes the pain from scratching makes your body release the pain-fighting chemical serotonin. It can make the itch feel even itchier.",

        "A running nose is the way our body flushes out germs from our nose while we catch cold and flu.",

        "On average, human body contains enough iron to make a nail 2.5 cm (1 inch) long.",

        "The JAVA language was previously called Oak. It was named after an oak tree that was grown outside James Gosling’s office (Gosling is best known as the founder and lead designer behind the Java coding language). Later, it was renamed Java, from Java coffee (a kind of coffee from Indonesia).",

        "It’s a common misbelief that the Firefox logo is a fox (I mean… it is in the name), but it is actually a red panda!",

        "There are around 700 separate programming languages",

        "PHP language was not created to be used worldwide for enterprise applications. Rasmus Lerdorf, the creator of PHP, built this coding language just to manage his personal web project! But, with time PHP became one of the popular programming languages in the market.",

        "According to many online studies, the most disliked programming languages are Perl, Delphi, and VBA",

        "Recent studies show that around 70% of coding jobs have nothing to do with technology at all",

        "The world’s first computer programmer was a renowned female mathematician",

        "Computer Programming was instrumental in helping end World War II",

        "Did you know? Fortran (FORmula TRANslation) was the first coding language created by John Backus and his team at IBM in the 1950s.",

        "There are 3 very different types of Hackers, one being malicious, the other benevolent, and the last somewhere in between the two",

        "The first-ever computer game made zero profit for its team of creators",

        "NASA still uses programs from the 70s in their spacecraft",

        "Coding is behind almost everything that is powered by electricity.",

        "In June 1991, James Gosling, Mike Sheridan, and Patrick Naughton began the Java language project. This programming language was originally created for interactive television, but this language was too modern for the digital cable television sector at that period.",

        "In this Covid-19 pandemic time, Avi Schiffmann, a teenager, developed one of the world’s most popular COVID-19 monitoring websites. He coded ncov2019.live, the admired covid virus tracker that is one of the most visited corona trackers across the globe.",

        "Did you know? ADA is a coding language built in 1980 that is used by the International Space Station. In 1995, this programming language was accepted as an international standard programming language.",

        "In 1980, MIT mathematician and a co-founder of the coding language LOGO, Seymour Papert published a book “Mindstorms: Children, Computers and Powerful Ideas” in 1980.  The book tells, kids can enhance their cognitive thinking skills by learning to code.",

        "Bill Gates, Co-founder of Microsoft, created his first computer program- an implementation of tic-tac-toe that enabled users to play games against the computer.",

        "Go is a programming language that was designed at Google in 2007 to enhance coding productivity in the time of multicore, networked machines and large codebases.",

        "Alan Turing invented the “Turing Test” (one of the Turing tests is CAPTCHA). It is a set of twisted numbers or words on online forms that assist to identify humans from computers. This CAPTCHA was also the contribution of Alan Turing.",

        "Markus Persson, a Swedish software programmer, developed and launched the computer game- Minecraft in 2009. Later the computer game became so popular that Microsoft bought the game for \$2.5 billion. ",

        "Steve Jobs and his partner Steve Wozniak began their career by building a computer arcade game named “Breakout.”",

        "Before creating the “C”, one of the most well-known programming languages, there was a predecessor coding language known as the “B”. This “B” language was written by Ken Thompson, a Turing Award-winning computer scientist. Later, Dennis Ritchie created the “C” language by improving the “B” language.",

        "Fred Cohen is known as the inventor of computer virus defense techniques. In 1983, he developed a parasitic application that could ‘infect’ computer systems. He called it a computer virus.",

        "Java is both a compiler and an interpreter programming language. The reason being, source code is first compiled into a binary byte-code. After that, byte-code runs on the Java Virtual Machine (a software-based interpreter).",

        "JavaScript programming language is called a dynamically typed language because a programmer can create new variables at runtime, and variable type is set at runtime.",

        "Python programming language for Data Science returns more than one value from a single function unlike in the majority of the modern programming languages.",

        "You cannot represent infinity as an integer in any coding language. But with python, you can represent an infinite integer via float values.",

        "Mercury was named for the swift-running Roman messenger god Mercury, because the planet is the fastest in the race around the sun. A year on Mercury is 88 Earth days, the shortest orbit in the solar system.",

        "Mercury's mantra is “the days are long but the years are short.” One day-night cycle on Mercury can take 176 Earth days — or more than two Mercury years — because of the planet's slow roll and elliptical orbit.",

        "Mercury is not the hottest planet in the solar system, despite being the closest to the sun! Turns out Mercury is both extremely hot and and extremely cold: Daytime temperatures on the side of the planet facing the sun reach 800 degrees Fahrenheit, but the side facing away from the sun gets all the way down to minus 290 degrees Fahrenheit.",

        "Believe it or not, scientists have found actual ice on Mercury! Because the planet has very little atmosphere (thanks to its proximity to the sun) and its axis has very little tilt (less than one degree) there are permanently-shadowed craters at Mercury's north and south poles that contain ice.",

        "Due to its thick atmosphere of toxic gases, Venus retains heat; its temperatures of 900 degrees Fahrenheit are the hottest in the solar system. Of course the planet named for the Roman god of love and beauty is a hottie!",

        "Days on Venus are loooooooong — it takes 243 Earth days for Venus to fully rotate. A Venusian day is also slightly longer than one Venusian year, which lasts 225 Earth days.",

        "It’s always backwards day on Venus, because the planet spins from east to west, the opposite direction from most of the other planets that orbit the sun.",

        "The surface of Venus is covered with mountains and volcanoes, some of which might still be active. It’s hard to get to the surface of Venus, though, because the air pressure is so intense it could crush you!",

        "Earth is unusual in our solar system for several reasons: It’s the only planet with liquid water at its surface, with known life, and with one moon. It’s also the only planet not named after a Roman god: The word “earth” comes from the Old English and Germanic word for “ground.”",

        "The earth is actually tilted 23 degrees. This tilt is why we have seasons: When the northern hemisphere is tilted toward the sun, we get summer; when it’s tilted away, we have winter.",

        "Earth’s moon is the fifth largest moon in the entire solar system! It’s useful, too, causing the tides and stabilizing the planet’s wobble, which creates a more predictable climate.",

        "It takes roughly eight minutes for light to reach Earth from the sun.",

        "Mars boasts the solar system’s largest known volcano, Olympus Mons (named for Mount Olympus, home of the gods). It’s three times the height of Mount Everest. ",

        "The Valles Marineris (seen above) is a Martian canyon that’s 10 times the size of the Grand Canyon. It is 3,000 miles long, 200 miles wide, and 4.3 miles deep (at its widest and deepest spots). For comparison, that's longer than the entire United States!",

        "Mars actually has two lumpy moons named Phobos and Deimos. Phobos isn’t holding up so well, and scientists predict it could crash into the planet or break up into a ring around the planet. Don’t wait up for it, though — it won’t happen for another 40 or 50 million years.",

        "The planet’s blood-red color — caused by the oxidation (rusting) of iron in its soil — earned it the name Mars after the bloodthirsty Roman god of war.",

        "Jupiter is the biggest planet in the solar system and therefore named for the king of the Roman gods. If you combined the mass of all the other planets, then doubled it, Jupiter would still be bigger. Put another way, you could fit 11 Earths along Jupiter’s equator.",

        "Jupiter’s signature stripey look is actually made of clouds of ammonia and water. Its Great Red Spot is a huge elliptical storm system that’s lasted for hundreds of years, but may turn circular and dissolve at some point in the future.",

        "The gas giant planet may not actually have a solid surface, like our planet does. If it does have a secret inner core, scientists believe it’s likely teeny — about the size of one little Earth.",

        "If Venus has the longest day, Jupiter has the shortest in the solar system. It makes a full rotation once every dizzying 10 hours.",

        "Saturn is known for its seven large rings. The rings are made from ice, rocks, and dust, ranging from grain size to house size. Over time, the rings will break apart and eventually disappear, as the particles collide and get sent farther out into space or fall down to the planet.",

        "The least dense of all the planets, Saturn would float in water if you could find a way to set it in a ginormous swimming pool.",

        "Saturn leads the solar system moon count with 82 moons!",

        "Ancient astronomers discovered Saturn because it can be seen with the naked eye. It’s the farthest planet we can easily see without a telescope.",

        "Saturn has a hexagonal-shaped jet stream located at its north pole, containing massive storms and cloud formations. This unique weather system has yet to be found anywhere else in our solar system!",

        "Uranus appears blue-green in color due to the methane gas in its atmosphere. (Cue the fart jokes.) ",

        "The planet is quite contrary, rotating not only east to west like Venus, but also on its side.",

        "Uranus was the first planet to be discovered via telescope. You can technically see it with the naked eye, but it’s very difficult.",

        "While other planets and moons are named for mythological characters, Uranus’s 27 moons (among them Titania, Oberon, Puck, and Juliet), take their names from the writings of William Shakespeare and Alexander Pope.",

        "Uranus has two sets of rings, totaling 13 rings in all, but they aren’t as visually striking as Saturn’s.",

        "Ice giant Neptune, like Uranus, looks blue because of the methane in its atmosphere. Its color is likely why it was named for the Roman god of the sea.",

        "Neptune is the only planet humans cannot see with the naked eye. In fact, its existence was predicted by mathematical equations before scientists found it with a telescope. (Remember that the next time you’re bored in math class!)",

        "Neptune and dwarf planet Pluto like to cross paths. Because Pluto’s orbit is elliptical, sometimes it’s closer to the sun and sometimes Neptune is. It takes 165 Earth years for Neptune to circle the sun. In fact, Neptune has only completed one orbit since it was discovered in 1846.",

        "The windiest planet in the solar system, Neptune can experience winds of more than 1,200 miles per hour. Compare that to Earth’s worst storms that top out at 250 miles per hour.",

        "Neptune has 14 moons. One of Neptune's moons, Triton, is unique in the solar system because it’s a large moon that orbits a planet in the opposite direction from the planet’s own rotation. (Other planets have small moons that do this, but not large ones.) This is called a retrograde orbit.",

        "Think Neptune is cold because it’s so far from the sun? You’re right — but scientists speculate that there could be an extremely hot ocean beneath the planet’s cold atmosphere.",

        "The world record for the most consecutive push-ups is 10, 507. It was set by Minoru Yoshida from Japan in 1980.",

        "According to the World's Sports Encyclopedia, there are over 8000 sports!",

        "he first sport as we know it today was most likely wrestling, and it originated in Greece in 776BC.",

        "The diameter of a basketball hoop was once almost double the size of a basketball, meaning that two balls could fit in the net side by side! However, this was changed in 2015 and the hoop is now much smaller.",

        "Modern swimsuits have become so advanced that some of them move faster in water than human skin. Some people think that the more a swimsuit covers your body, the greater your advantage when swimming in the water.",

        "Sheffield FC is the world's oldest football club, founded in 1857.",

        "Tennis strings were traditionally made from the intestines of goats, cows, or sheep. Fibres were extracted from the intestines which contain collagen, responsible for the strings' elasticity. Nowadays they are mostly made from nylon.",

        "Olympic Gold Medals are made, mostly, of sterling silver, not gold. In fact, they haven't been made of pure gold since 1912!",

        "Golf balls have around 336 dimples. That's the average, but there's no limit to the number of dimples. There can be between 300 and 500. Their purpose is to make them travel further in the air.",

        "Ski ballet was once a competitive sport. It was even a demonstration sport in the 1988 and 1922 Winter Olympic games.",

        "Major League Baseball umpires are obliged to wear black underwear during games, in case their trousers split!",

        "The average lifespan of a Minor League Baseball ball is five to seven pitches.",

        "The phrase about winning 'hands down' was initially in reference to a jockey, who would win a race without pulling the reins of his horse or whipping his horse.",

        "The average distance a person walks in their lifetime is four times around the whole world.",

        "There are only eighteen minutes of action in the average baseball game.",

        "The grass at Wimbledon used to be around 5cm long until an English player was bitten by a snake in 1949. Now, it's 8mm long.",

        "Sports have been played in outer space - yes, a golf ball was hit by Alan Shephard, and a javelin thrown by Edgar Mitchell on the moon, in 1971.",

        "In the 1928 Olympics, Australian rower Bobby Pearce won his race against eight others, even though he stopped to let ducks pass him during the race.",

        "It is customary for jockeys to be paid in coins, no matter how much they win!",

        "The Stanley cup was originally two storeys tall before people realised it was a bit too difficult to transport.",

        "The fastest hat trick in National Hockey League history took 21 seconds. Bill Mosienko set the record in 1952.",

        "The most points ever scored in a basketball player's career is 38,387, by Kareem Abdul-Jabbar of the United States.",

        "Ben Smith holds the world record for the most consecutive marathons, having run 401 marathons in 401 days, covering 10,506 miles altogether.",

        "The longest boxing match in history lasted 110 rounds and went on for over seven hours. It was played in 1893 by Andy Bowen and Jack Burke.",

        "The longest tennis match lasted eleven hours and 5 minutes. It was played by John Isner of the USA and Nicolas Mahut from France.",

        "The world record for the longest bow and arrow shot us held by Matt Stutzman from the USA, with a distance of 283.464 metres.",

        "Rocky Marciano holds the career heavyweight boxing record, with 49 victories, 43 of them being knockouts. This record was set between 1948 and 1956, without any losses.",

        "The colours of the Olympic rings represent the flags of every country competing at the Olympics in 1912, the year that they were designed. They were designed by Frenchman and founder of the International Olympic Committee, Baron Pierre de Coubertin.",

        "Martina Navratilova has won six Wimbledon titles in a row. Unbeaten in this streak, she set the record between 1982 and 1987.",

        "Byron Nelson has won more Professional Golf Association tours back-to-back than any other professional golfer. In 1945, he won 11.",

        "In the year 1959, Pelé scored 127 goals, more than any other football player in a single calendar year.",

        "The USA's Connecticut Women's basketball team once won 90 games in a row. Not a single game was lost between the start of the 2008-2009 season and the end of 2010.",

        "The Professional Golfer's Association record for the highest score on a Par-4 is 16, and was set in 2011 by Kevin Na, a Korean American professional golfer.",

        "The Professional Golfer's Association record for the highest score on a Par-4 is 16, and was set in 2011 by Kevin Na, a Korean American professional golfer.",

        "Bowling was invented around 3200BC in Egypt.",

        "The oldest sports in the world are thought to be wrestling, running, javelin throwing, polo and hockey.",

        "The most dangerous sports in the world are thought to be running, scuba diving, canoeing, Grand Prix racing, and extreme mountain climbing.",

        "Almost all of ordinary matter (99.9999999% of it) is empty space. If you took out all of the space in our atoms, the entire human race (all 7 billion of us) would fit into the volume of a sugar cube.",

        "Many of the atoms you're made of, from the calcium in your bones to the iron in your blood, were brewed up in the heart of an exploding star billions of years ago.",

        "In fact, your body contains cosmic relics from the creation of the universe. Almost all of your hydrogen atoms were formed in the Big Bang, about 13.7 billion years ago.",

        "And back in the days before digital television, if you tuned your TV between stations, a small percentage of the static you would see would actually be the afterglow of the Big Bang.",

        "Light from some stars takes so long to travel to our eyes that when you look at the star-speckled night sky you're actually peering deep into the past. NASA's Hubble Telescope can look as far back as 13 billion years ago.",

        "Forty-seven years ago, humankind took its first steps on the moon — and the bootprints will probably still be there a million years from now. That's because the moon has no atmosphere, so there's no wind or water to sweep through and erase the marks.",

        "Outer space is silent. Eerily silent. That's because sound waves need some sort of medium to travel through. And space is a vacuum. A dark, silent vacuum.",

        "If you touch two pieces of the same type of metal together in the vacuum of space, they will fuse, bound together until eternity (or until you break them apart).",

        "One year on Venus is equal to 224 Earth days. And one day on Venus is equal to 243 Earth days. Which makes a day on Venus longer than a year. And to top it off, Venus is the only planet in our solar system that rotates backwards.",

        "The sun makes up 99.86% of the mass of the solar system. It's so big that you could squeeze 1.3 million Earths inside of it.",

        "There might be as many as three sextillion stars in the universe. That's 3 followed by 23 zeros, or 300,000,000,000,000,000,000,000. That's more than all of the grains of sand on Earth.",

        "When a massive star explodes, its scrunched up core forms something called a neutron star. Neutron stars are so dense that just a teaspoon of their material would weigh more than Mt. Everest. The explosion can spin the neutron star to mind blowing speeds — up to 600 rotations per second.",

        "Ordinary, observable matter (like stars and planets) makes up a measly 5% of the universe. The other 95% universe is made up of invisible dark energy (68%) and dark matter (27%). That means there's 95% of the universe that we don't know about yet.",

        "It is impossible to predict just how many stars are there in our universe. Right now, as per the scientists and astronomers, there is an estimate of 200 to 400 billion stars in the milky way itself. With this, there are billions of galaxies in our universe.",

        "After 164BC there was a continuous recording of the comet each time it was visible. The next Halley's comet will be seen from earth in the year 2061.",

        "The cost of the NASA space suit costs for around \$12,000,000. However, 70 percent of the total cost is for the backpack and control module.",

        "Astronomers have found a massive water vapor cloud which holds 140 trillion times the mass of water in the Earth's oceans somewhere around 10 billion light years away. It is the largest discovery of water ever found.",

        "Every year the moon is drifting away from Earth by 3.8 cm. Scientists do believe that eventually, the Moon will move out of the field of Earth's Gravity. However, this won't happen for the next billions of years to come.",

        "The first ever black hole photo which was released in April 2019 shows a halo of dust and gas. It is stated to be 310 million trillion miles from earth. The photo was captured by the Event Horizon Telescope, a network of eight linked telescopes, and was also captured due to the algorithm of programmer Katie Bowman.",

        "The Sun accounts for 99.86% of the mass in our solar system with a mass of around 330,000 times that of Earth. Did you know that the Sun is made up of mostly hydrogen (three quarters worth) with the rest of its mass attributed to helium.  If the Sun had a voice would it be high and squeaky from all that helium?",

        "The universe extends far beyond our own galaxy, The Milky Way, which is why scientists can only estimate how many stars are in space.  However, scientists estimate the universe contains approximately 1,000,000,000,000,000,000,000,000 stars, or a septillion.   While no one can actually count every single grain of sand on the earth, the estimated total from researchers at the University of Hawaii, is somewhere around seven quintillion, five hundred quadrillion grains.  That is an awfully big sand castle!",

        "There’s a planet made of diamonds twice the size of earth The \"super earth,\" aka 55 Cancri e, is most likely covered in graphite and diamond.  Paying a visit to that planet would probably pay for the \$12 million dollar space suit needed to get there!",

        "In the beginning, the universe was infinitely hot – minutes after its birth estimates suggest that it had around 1 billion Kelvin degrees.",

        "As the universe grew old, it also became much colder. Current average temperatures are around 2.725 degrees, Kelvin.",

        "The exact size of the universe is impossible to calculate. However, the observable universe is around 93 billion light-years with a radius of around 46.5 billion light-years.",

        "The universe is made up of 4.9% ordinary (baryonic) matter, 26.8% dark matter, and 68.3% dark energy. Considering only the largest structures, the universe is made up of filaments, voids, superclusters, and galaxy groups, and clusters.",

        "Dark matter and dark energy are invisible theoretical matter. The only proof of their existence lies in certain phenomena which mostly tell us that huge chunks of mass are missing, thus, in a way, this justifies their probable existence.",

        "The age of the universe is predicted to be around 13.8 billion years old.",

        "The universe is not only expanding, but it is also accelerating its expansion. Galaxies move away from each other but at the same time, space is also moving/expanding.",
        )
}