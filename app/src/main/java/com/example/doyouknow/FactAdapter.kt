package com.example.doyouknow


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.doyouknow.databinding.ItemLayoutBinding
import com.example.doyouknow.facts.favorites

class FactAdapter: RecyclerView.Adapter<FactAdapter.FactHolder>() {
    val factsList = ArrayList<Fact>()
    class FactHolder(item: View): RecyclerView.ViewHolder(item) {
        val binding = ItemLayoutBinding.bind(item)
        fun bind(fact: Fact) {
            binding.factText.text = fact.text
            if(favorites.favoritesFactsTextList.contains(fact.text)) {
               binding.addToFavIcon.setImageResource(R.drawable.favorite_checked)
            } else  binding.addToFavIcon.setImageResource(R.drawable.favorite_unchecked)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FactHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        return FactHolder(view)
    }

    override fun onBindViewHolder(holder: FactHolder, position: Int) {
        holder.bind(factsList[position])
    }

    override fun getItemCount(): Int {
        return factsList.size
    }
    fun addFact(fact: Fact) {
        factsList.add(fact)
    }
}